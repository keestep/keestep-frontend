## To Do
### Authentication

| Task          | Status        |
| ------------- |:-------------:|
| Login         | Done          |
| Logout        | Done          |
| Store token & use for APIs   | Done          |
| Persist a session         | Done          |

### Admin

| Task          | Status        |
| ------------- |:-------------:|
| Create        | Done          |
| Update        | Done          |
| Get           | Done          |

### Faculty

| Task          | Status        |
| ------------- |:-------------:|
| Create        | pending       |
| Update        | pending       |
| Get           | pending       |

### Students

| Task          | Status        |
| ------------- |:-------------:|
| Create        | pending       |
| Update        | pending       |
| Get           | pending       |

### Timetable

| Task          | Status        |
| ------------- |:-------------:|
| Create        | pending       |
| Update        | pending       |
| Get           | pending       |

### Social Media

| Task          | Status        |
| ------------- |:-------------:|
| Create        | pending       |
| Update        | pending       |
| Get           | pending       |
| Like          | pending       |
| Comment       | pending       |
| Edit comment  | pending       |
| Delete comment| pending       |

### Queries

| Task          | Status        |
| ------------- |:-------------:|
| Create        | pending       |
| Update        | pending       |
| Get           | pending       |
| Reply to thread       | pending       |

### NoticeBoard

| Task          | Status        |
| ------------- |:-------------:|
| Create        | pending       |
| Update        | pending       |
| Get           | pending       |

### College Profile

| Task          | Status        |
| ------------- |:-------------:|
| Update        | Done          |
| Get           | Done          |

### User Profile

| Task          | Status        |
| ------------- |:-------------:|
| Update        | pending       |
| Get           | Done          |

### Notifications

``` Planned for beta release ```

| Task          | Status        |
| ------------- |:-------------:|
| Create        | pending       |
| Update        | pending       |
| Get           | pending       |

### Error Handling

| Task          | Status        |
| ------------- |:-------------:|
| API errors    | Done          |
| Process errors| pending       |