---
nav:
  title: Components
  path: /components
---

## Foo

Demo:

```tsx
import React from 'react';
import { Foo } from 'dumi';

const Bar = () => <Foo title="First Demo" />

export default Bar;
```

More skills for writing demo: https://d.umijs.org/guide/demo-principle
