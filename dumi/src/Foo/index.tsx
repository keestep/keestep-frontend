import React from 'react';

export const Foo = ({ title }: { title: string }) => <h1>{title}</h1>;