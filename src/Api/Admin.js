import API from '../Utility/api';

export const createAdmin = async (data) => {
  try {
    const res = await API.request({
      method: 'POST',
      url: '/admin',
      data
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false, message: error.response };
  }
};

export const getAdmins = async (params) => {
  try {
    const res = await API.request({
      method: 'GET',
      url: '/admin',
      params
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false };
  }
};

export const getAdmin = async (id) => {
  try {
    const res = await API.request({
      method: 'GET',
      url: `/admin/${id}`,
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false };
  }
};

export const updateAdmin = async (id, data) => {
  try {
    const res = await API.request({
      method: 'PUT',
      url: `/admin/${id}`,
      data
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false };
  }
};