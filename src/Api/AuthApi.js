import { message } from 'antd';

import API from '../Utility/api';
import { ACCESS_KEY_CONSTANT, CURRENT_USER_CONSTANT, USER_ROLES } from '../Utility/constant';
import { getStorageItem, setStorageItem, clearStorage } from '../Utility/storage';

const getCurrentProfile = async () => {
  try {
    const access_token = getStorageItem(ACCESS_KEY_CONSTANT);
    const { data } = await API.request({
      method: 'GET',
      url: '/me',
      headers: {
        Authorization: `Bearer ${access_token}`
      }
    })
    if (data) {
      setStorageItem(CURRENT_USER_CONSTANT, JSON.stringify(data));
      return { success: true, data };
    }
  } catch (e) {
    return { success: false, message: e.message };
  }
}

export const getCurrentUser = () => {
  const userObj = getStorageItem(CURRENT_USER_CONSTANT);
  if(userObj) {
    const profile = JSON.parse(getStorageItem(CURRENT_USER_CONSTANT));
    return { 
      data: {
        isAdmin: profile.role === USER_ROLES.admin,
        ...profile
      }
    };
  } else {
    message.error("User info not found. Please logout and login agian")
  }
}

export const adminLogin = async ({ email, password }) => {
  try {
    const { data } = await API.request({
      method: 'POST',
      url: '/admin/login',
      data: { email, password }
    });
    if (data) {
      setStorageItem(ACCESS_KEY_CONSTANT, data && data.access_token);
      await getCurrentProfile();
      return true;
    }
    return false;
  } catch (error) {
    return false;
  }
};

export const facultyLogin = async ({ email, password }) => {
  try {
    const { data } = await API.request({
      method: 'POST',
      url: '/faculty/login',
      data: { email, password }
    });
    if (data) {
      setStorageItem(ACCESS_KEY_CONSTANT, data && data.access_token);
      return true;
    }
    return false;
  } catch (error) {
    return false;
  }
};

export const Logout = async () => {
  try {
    message.info("Logging you out...")
    clearStorage();
    window.location.href = "/login";
  } catch (e) {
    message.error(e.message)
  }
};
