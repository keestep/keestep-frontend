import API from '../Utility/api';

export const createBook = async (data) => {
  try {
    const res = await API.request({
      method: 'POST',
      url: '/books',
      data
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false, message: error.response };
  }
};

export const getBooks = async (params) => {
  try {
    const res = await API.request({
      method: 'GET',
      url: '/books',
      params
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false };
  }
};

export const updateBook = async (id, data) => {
  try {
    const res = await API.request({
      method: 'PUT',
      url: `/books/${id}`,
      data
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false };
  }
};