import API from '../Utility/api';
import { ACCESS_KEY_CONSTANT } from '../Utility/constant';
import { getStorageItem } from '../Utility/storage';

export const getCourses = async () => {
  try {
    const access_token = getStorageItem(ACCESS_KEY_CONSTANT);
    const { data } = await API.request({
      method: 'GET',
      url: '/course',
      headers: {
        Authorization: `Bearer ${access_token}`
      }
    })
    return { success: true, data };
  } catch (e) {
    return { success: false, message: e.message };
  }
}
