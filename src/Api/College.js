import API from '../Utility/api';

export const getCollege = async () => {
  try {
    const res = await API.request({
      method: 'GET',
      url: '/college',
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false, message: error.message };
  }
};

export const updateCollege = async (id, data) => {
  try {
    const res = await API.request({
      method: 'PUT',
      url: `/college/${id}`,
      data
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false, message: error.message };
  }
};
