import API from '../Utility/api';

export const createCourse = async (data) => {
  try {
    const res = await API.request({
      method: 'POST',
      url: '/course',
      data
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false, message: error.response };
  }
};

export const getCourses = async (params) => {
  try {
    const res = await API.request({
      method: 'GET',
      url: '/course',
      params
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false, message: error.message };
  }
};

export const updateCourse = async (id, data) => {
  try {
    const res = await API.request({
      method: 'PUT',
      url: `/course/${id}`,
      data
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false, message: error.response };
  }
};