import API from '../Utility/api';

export const createFaculty = async (input) => {
  try {
    const { data } = await API.request({
      method: 'POST',
      url: '/faculty',
      data: input
    });
    return { success: true, data };
  } catch (error) {
    return { success: false, message: error.message };
  }
};

export const getFacultys = async (params) => {
  try {
    const { data } = await API.request({
      method: 'GET',
      url: '/faculty',
      params
    });
    return { success: true, data };
  } catch (error) {
    return { success: false, message: error.message };
  }
};

export const updateFaculty = async (id, input) => {
  try {
    const { data } = await API.request({
      method: 'PUT',
      url: `/faculty/${id}`,
      data: input
    });
    return { success: true, data };
  } catch (error) {
    return { success: false, message: error.message };
  }
};