import API from '../Utility/api';

export const createNotice = async (data) => {
  try {
    const res = await API.request({
      method: 'POST',
      url: '/noticeboard',
      data
    });
    return ({ success: true, data: res.data });
  } catch (e) {
    return { success: false, message: e.message };
  }
};

export const getNotices = async () => {
  try {
    const res = await API.request({
      method: 'GET',
      url: '/noticeboard',
    });
    return ({ success: true, data: res.data });
  } catch (e) {
    return { success: false, message: e.message };
  }
};

export const updateNotice = async (id, data) => {
  try {
    const res = await API.request({
      method: 'PUT',
      url: `/noticeboard/${id}`,
      data
    });
    return ({ success: true, data: res.data});
  } catch (error) {
    return { success: false, message: error.message };
  }
}
