import API from '../Utility/api';

export const getNotifications = async (params) => {
  try {
    const { data } = await API.request({
      method: 'GET',
      url: '/notifications',
      params
    })
    if (data) {
      return { success: true, data };
    }
  } catch (e) {
    return { success: false, message: e.message };
  }
}

export const getNotificationCount = async () => {
  try {
    const { data } = await API.request({
      method: 'GET',
      url: '/notifications/count',
    })
    if (data) {
      return { success: true, data };
    }
  } catch (e) {
    return { success: false, message: e.message };
  }
}

export const markAsRead = async (id) => {
  try {
    const { data } = await API.request({
      method: 'PUT',
      url: `/notifications/${id}/mark-as-read`,
    })
    if (data) {
      return { success: true, data };
    }
  } catch (e) {
    return { success: false, message: e.message };
  }
}