import API from '../Utility/api';

export const createQuery = async (data) => {
  try {
    const res = await API.request({
      method: 'POST',
      url: '/queries',
      data
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false, message: error.response };
  }
};

export const getQueries = async (params) => {
  try {
    const res = await API.request({
      method: 'GET',
      url: '/queries',
      params
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false };
  }
};

export const getQuery = async (id) => {
  try {
    const res = await API.request({
      method: 'GET',
      url: `/queries/${id}`,
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false };
  }
};

export const replyToQuery = async (id, data) => {
  try {
    const res = await API.request({
      method: 'PUT',
      url: `/queries/${id}/reply`,
      data
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false };
  }
};

export const deleteReplyToQuery = async (id, replyId) => {
  try {
    const res = await API.request({
      method: 'DELETE',
      url: `/queries/${id}/reply/${replyId}`,
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false };
  }
};

export const updateQuery = async (id, data) => {
  try {
    const res = await API.request({
      method: 'PUT',
      url: `/queries/thread/${id}`,
      data
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false };
  }
};