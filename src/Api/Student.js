import API from '../Utility/api';

export const createStudent = async (input) => {
  try {
    const { data } = await API.request({
      method: 'POST',
      url: '/students',
      data: input
    });
    return { success: true, data };
  } catch (error) {
    return { success: false, message: error.message };
  }
};

export const getStudents = async () => {
  try {
    const { data } = await API.request({
      method: 'GET',
      url: '/students',
    });
    return { success: true, data };
  } catch (error) {
    return { success: false, message: error.message };
  }
};

export const updateStudent = async (id, data) => {
  try {
    const res = await API.request({
      method: 'PUT',
      url: `/students/${id}`,
      data
    });
    return ({ success: true, data: res.data});
  } catch (error) {
    return { success: false, message: error.message };
  }
};