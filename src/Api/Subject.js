import API from '../Utility/api';

export const createSubject = async (data) => {
  try {
    const res = await API.request({
      method: 'POST',
      url: '/subjects',
      data
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false, message: error.response };
  }
};

export const getSubjects = async (params) => {
  try {
    const res = await API.request({
      method: 'GET',
      url: '/subjects',
      params
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false };
  }
};

export const updateSubject = async (id, data) => {
  try {
    const res = await API.request({
      method: 'PUT',
      url: `/subjects/${id}`,
      data
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false };
  }
};