import API from '../Utility/api';

export const getUser = async (id) => {
  try {
    const res = await API.request({
      method: 'GET',
      url: `/user/${id}`,
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false, message: error.message };
  }
};

export const updateUser = async (id, data) => {
  try {
    const res = await API.request({
      method: 'PUT',
      url: `/user/${id}`,
      data
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false, message: error.message };
  }
};

export const getNews = async () => {
  try {
    const res = await API.request({
      method: 'GET',
      url: `/news`,
    });
    return { success: true, data: res.data };
  } catch (error) {
    return { success: false, message: error.message };
  }
};