import React, { useContext } from 'react';
import { ThemeProvider } from 'styled-components';
import { BrowserRouter as Router, Switch, Route, useHistory } from 'react-router-dom';
import { lightTheme, darkTheme } from './Utility/theme.config';
import { ThemeContext } from './context/themeContext';
import { AuthContext } from './context/Context';
import './App.css';

import Home from './Containers/Home/index';
import { Login } from './Containers/Login';
import { UnAuthorized } from './Components/UnAuthorized';
import { MobileView, NotFound } from './Components';
import { PrivateRoute } from './Components/PrivateRoute';

const App = () => {
  
  const themeContext = useContext(ThemeContext);
  const { authenticated } = useContext(AuthContext);
  const history = useHistory();
  const { theme } = themeContext;
  const isLightTheme = theme === 'light';
  const isMobileOrTablet = window.innerWidth < 768;

  if(isMobileOrTablet) {
    return <MobileView />;
  }

  return (
    <AuthContext.Provider value={{ authenticated }}>
      <ThemeProvider theme={isLightTheme ? lightTheme : darkTheme}>
        <Router>
          <Switch>
            <Route exact path="/login" component={Login} history={history} />
            <Route exact path="/unauthorized" component={UnAuthorized} history={history} />
            <PrivateRoute path="/" component={Home} />
            <Route path="*" component={NotFound} />
          </Switch>
        </Router>
      </ThemeProvider>
    </AuthContext.Provider>
  );
};

export default App;
