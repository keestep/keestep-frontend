import React, { useState } from "react";
import styled from 'styled-components';
import { Typography, Popconfirm, Row, Col, Card, notification } from "antd";
import { EditFilled, DeleteFilled } from "@ant-design/icons";

import { AdminForm } from "../Containers/Users/components/AdminForm";
import thumbnail from "../Assets/Img/thumb-1.jpg";

import { updateAdmin } from "../Api/Admin";

const { Title } = Typography;

export const AdminTable = ({ data, profile, changed }) => {

  const { Meta } = Card;

  const [adminFormVisible, setAdminFormVisible] = useState(false);

  const deleteAdmin = async (id) => {
    if(profile._id === id) {
      notification.warn({ message: "You cannot delete yourself when logged in" })
    } else {
      const res = await updateAdmin(id, { deleted: true })
      if(res.success) {
        notification.success({ message: "Admin has been deleted" });
        changed(0);
      }
    }
  }

  const cardActions = (id) => [
    <EditFilled key="edit" onClick={() => setAdminFormVisible(true)} />,
    (
      <Popconfirm
        title="Are you sure you want to delete?"
        onConfirm={() => deleteAdmin(id)}
      >
        <DeleteFilled key="delete" />
      </Popconfirm>
    ),
  ]

  return (
    <Container>
      <StyledRow gutter={[0, 16]}>
        {data.map((user) => {

          const { _id, name, email, photo_url } = user;

          return (
            <Col span={6}>
              <Card
                hoverable
                style={{ width: 250, height: 350 }}
                cover={(
                  <div
                    style={{
                      width: 250,
                      height: 220,
                      backgroundImage: `url(${photo_url || thumbnail})`,
                      backgroundSize: 'cover'
                    }}
                  />
                )}
                actions={cardActions(_id)}
              >
                <Meta
                  title={<Title level={4}>{name}</Title>}
                  description={email}
                />
              </Card>
              <AdminForm
                editMode
                data={user}
                visible={adminFormVisible}
                onClose={() => setAdminFormVisible(false)}
                changed={changed}
              />
            </Col>
          )}
        )}
      </StyledRow>
    </Container>
  );
};


const Container = styled.div`
  width: 100%;
`;

const StyledRow = styled(Row)`
  margin: 50px 0;
`;