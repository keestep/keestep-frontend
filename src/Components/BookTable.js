import React, { useState } from "react";
import styled from 'styled-components';
import { Table, Typography, Popconfirm } from "antd";
import { DeleteFilled } from "@ant-design/icons";

import { BranchForm } from "../Containers/HomeDashboard/components/BranchForm";

const { Title, Link } = Typography;

export const BranchTable = ({ data }) => {

  const [branchFormVisible, setBranchFormVisible] = useState(false);

  const columns = [
    {
      title: "Branch name",
      dataIndex: "branch_name",
      key: "branch_name",
      render: (text) => (
        <Link onClick={() => setBranchFormVisible(true)}>{text}</Link>
      )
    },
    {
      title: "Course Name",
      dataIndex: "course_name",
      key: "course_name",
    },
    {
      title: "Branch Code",
      dataIndex: "branch_code",
      key: "branch_code",
    },
    {
      title: "Year",
      dataIndex: "year",
      key: "year",
    },
    {
      title: "Section",
      dataIndex: "section",
      key: "section",
    },
    {
      title: "Take action",
      render: (_, record) => (
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Popconfirm title="Sure to delete?" onConfirm={() => null}>
            <DeleteFilled />
          </Popconfirm>
        </div>
      )
    }
  ];

  return (
    <Container>
      <Title level={3}>Branches</Title>
      <Table dataSource={dataSource} columns={columns} />
      <BranchForm id={2} visible={branchFormVisible} onClose={() => setBranchFormVisible(false)} />
    </Container>
  );
};


const Container = styled.div`
  width: 100%;
`;