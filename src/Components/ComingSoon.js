import React from 'react';
import { Result } from 'antd';

import ComingSoonSVG from '../Assets/Img/in_progress.svg';

export default function ComingSoon () {
  return (
    <Result
      icon={<img src={ComingSoonSVG} width={400} alt="coming soon" />}
      title="This page is coming soon"
      subTitle="We're building the best for you"
    />
  );
}
