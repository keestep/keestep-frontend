import React, { useState, useEffect } from "react";
import styled from 'styled-components';
import { Table, Typography, Popconfirm, Tag, Button, Row, notification } from "antd";
import { EditFilled, DeleteFilled } from "@ant-design/icons";

import { CourseForm } from "../Containers/HomeDashboard/components/CourseForm";

import { TableTitle } from "../Components/TableTitle";
import { getCourses, updateCourse, createCourse } from "../Api/Course";

const { Title, Link } = Typography;

export const CourseTable = ({ isAdmin }) => {

  const [courseFormVisible, setCourseFormVisible] = useState(false);
  const [courseData, setCourseData] = useState([]);
  const [formData, setFormData] = useState(null);
  const [editMode, setEditMode] = useState(false);

  const fireCoursesApi = async () => {
    const res = await getCourses();
    if(res.success) {
      setCourseData(res.data);
    } else {
      notification.error({
        message: "Error fetching courses",
        description: res.message
      })
    }
  }

  const deleteCourse = async (id) => {
    const res = await updateCourse(id, { delete: true });
    if(res.success) {
      setFormData(res.data);
    } else {
      notification.error({
        message: "Error deleting courses",
        description: res.message
      })
    }
  }

  const handleEdit = (data) => {
    console.log(data)
    setCourseFormVisible(true);
    setFormData(data);
  }

  useEffect(() => {
    fireCoursesApi();
  }, [formData])

  const columns = [
    {
      title: "Course name",
      dataIndex: "course_name",
      key: "course_name",
      render: (text, r) => (
        <Link onClick={handleEdit}>{text}</Link>
      )
    },
    {
      title: "Branches",
      dataIndex: "branches",
      key: "branches",
      // render: (branches) => branches.map(branch => <Tag color="magenta">{branch}</Tag>)
    },
    {
      title: "Course Code",
      dataIndex: "course_code",
      key: "course_code",
    },
    {
      title: "Duration",
      dataIndex: "duration",
      key: "duration",
    },
    {
      title: "Intake",
      dataIndex: "year_of_intake",
      key: "year_of_intake",
    },
    {
      title: "Passout",
      dataIndex: "year_of_passout",
      key: "year_of_passout",
    },
    {
      title: "Credits",
      dataIndex: "credits",
      key: "credits",
    },
    {
      title: "Students",
      dataIndex: "student_count",
      key: "student_count",
    },
  ];

  const columnsWithActions = [
    ...columns,
    {
      title: "Take action",
      render: (_, data) => {
        const id = data?._id;
        return (
          <Row justify="space-around">
            <EditFilled onClick={() => handleEdit(data)} />
            <Popconfirm title="Sure to delete?" onConfirm={() => deleteCourse(id)}>
              <DeleteFilled />
            </Popconfirm>
          </Row>
        )
      }
    }
  ]

  return (
    <Container>
      <TableTitle
        title="Courses"
        totalRecords={courseData.length}
        btnTitle="+ Add Course"
        btnAction={() => setCourseFormVisible(true)}
        showAddCourse={isAdmin}
      />
      <Table
        columns={isAdmin ? columnsWithActions : columns}
        dataSource={courseData}
      />
      <CourseForm
        isAdmin={isAdmin}
        editMode={editMode}
        data={formData}
        visible={courseFormVisible}
        setFormData={setFormData}
        createAction={createCourse}
        updateAction={updateCourse}
        onClose={() => setCourseFormVisible(false)}
      />
    </Container>
  );
};


const Container = styled.div`
  width: 100%;
  margin: 30px 0;
`;