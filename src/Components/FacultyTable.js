import React, { useState } from "react";
import styled from 'styled-components';
import { Typography, Popconfirm, Row, Col, Card, message } from "antd";
import { EditFilled, DeleteFilled } from "@ant-design/icons";

import { FacultyForm } from "../Containers/Users/components/FacultyForm";
import thumbnail from "../Assets/Img/thumb-1.jpg";

import { updateFaculty } from "../Api/Faculty";

const { Title } = Typography;

export const FacultyTable = ({ data, profile }) => {

  const { Meta } = Card;

  const [facultyFormVisible, setFacultyFormVisible] = useState(false);

  const deleteFaculty = async (id) => {
    if(profile._id === id) {
      message.warn("You cannot delete yourself when logged in")
    } else {
      const res = await updateFaculty(id, { deleted: true })
      if(res.success) {
        message.success("Faculty has been deleted")
      } else {
        message.error(res.message)
      }
    }
  }

  const cardActions = (id) => [
    <EditFilled key="edit" onClick={() => setFacultyFormVisible(true)} />,
    (
      <Popconfirm
        title="Are you sure you want to delete?"
        onConfirm={() => deleteFaculty(id)}
      >
        <DeleteFilled key="delete" />
      </Popconfirm>
    ),
  ]

  return (
    <Container>
      <Title level={3}>Facultys</Title>
      <StyledRow gutter={[0, 16]}>
        {data.map((user) => {
          const { _id, name, email, photo_url } = user;
          return (
            <Col span={6}>
              <Card
                hoverable
                style={{ width: 250, height: 350 }}
                cover={(
                  <div
                    style={{
                      width: 250,
                      height: 220,
                      backgroundImage: `url(${photo_url || thumbnail})`,
                      backgroundSize: 'cover'
                    }}
                  />
                )}
                // cover={<img alt={name} src={!!photo_url ? photo_url : thumbnail} height={230}  />}
                actions={cardActions(_id)}
              >
                <Meta
                  title={<Title level={4}>{name}</Title>}
                  description={email}
                />
              </Card>
              <FacultyForm
                data={user}
                visible={facultyFormVisible}
                onClose={() => setFacultyFormVisible(false)}
                data={user}
              />
            </Col>
          )}
        )}
      </StyledRow>
    </Container>
  );
};


const Container = styled.div`
  width: 100%;
`;

const StyledRow = styled(Row)`
  margin: 50px 0;
`;