import React from 'react';
import styled from 'styled-components';
import { Row, Select, Space, Typography } from 'antd';

const { Text } = Typography;
const { Option } = Select;

export const FilterStrip = ({ filters }) => {
  
  return (
    <Row justify="end" style={{ padding: 30 }}>
      <Space size={30}>
        {filters.map(({ label, defaultValue, action, options }) => (
          <Space>
            <Text>{label}</Text>
            <SelectQueryStatus
              size="large"
              defaultValue={options[0].name}
              onChange={value => action(value)}
            >
              {options.map(({ name, value }) => (
                <Option key={value} value={value}>
                  {name}
                </Option>
              ))}
            </SelectQueryStatus>
          </Space>
        ))}
      </Space>
    </Row>
  )
}

const SelectQueryStatus = styled(Select)`
  width: 150px;
`;