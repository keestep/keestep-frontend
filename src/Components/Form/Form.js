import React, { useState } from 'react';
import styled from "styled-components";
import PropTypes from 'prop-types';
import { Form, Input, InputNumber, Button, Select, Switch } from 'antd';


export const FormComponent = ({
  name,
  width,
  onFinish,
  onFinishFailed,
  btnLoading,
  btnDisabled,
  submitButtonTitle,
  formItems = []
}) => {

  const { Item } = Form;

  const layout = {
    labelCol: { span: 16 },
    wrapperCol: { span: 24 },
  };

  const tailLayout = {
    wrapperCol: { offset: 0, span: 24 },
  };


  const renderFormItem = (type, props) => {
    switch (type) {
      case 'input':
        return <Input {...props} />;

      case 'number':
        return <Input type="number" {...props} />

      case 'switch':
        return <Switch {...props} />
      
      case 'checkbox':
        return <Input type="checkbox" {...props} />

      case 'select':
        return (
          <Select {...props} >
            {props.options.map(({ name, value }) => (
              <Select.Option value={name}>{value}</Select.Option>
            ))}
          </Select>
        )
      
      case 'textarea': 
        return (
          <Input.TextArea {...props} /> 
        )

      default:
        break;
    }
  }

  const validateMessages = {
    required: "'${label}' is required!",
  };


  return (
    <>
      <Form
        {...layout}
        name={name}
        layout="vertical"
        validateMessages={validateMessages}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        style={{ width }}
      >
        {formItems.map(({ initialValue, hidden, label, name, rules, type }) => (
          <Item
            key={label}
            initialValue={initialValue}
            label={label}
            name={name}
            rules={rules}
            hidden={hidden}
          >
            {renderFormItem(type.name, type.props)}
          </Item>
        ))}
        <Form.Item  {...tailLayout}>
          <Button
            block
            loading={btnLoading}
            disabled={btnDisabled}
            type="primary"
            htmlType="submit"
            size="large"
          >
            {submitButtonTitle}
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}

Form.propTypes = {
  formItems: PropTypes.array.isRequired,
}