import React from "react";
import styled from "styled-components";
import { Layout, Menu, Row, Avatar, Badge } from "antd";
import { BellTwoTone, LogoutOutlined, UserOutlined } from "@ant-design/icons";

import { Logout } from "../Api/AuthApi"

export const GlobalHeader = ({ profile, userFunction, notificationsFunction }) => {
  const { Item } = Menu;
  const { Header } = Layout;

  return (
    <Header style={{ width: '100%' }}>
      <Row justify="end">
        <Menu theme="dark" mode="horizontal">
          <Item
            onClick={userFunction}
            icon={!!profile ? <Avatar style={{ marginRight: 10 }} src={profile.photo_url} /> : <UserOutlined />}
            key="1"
          >
            {profile && profile.name}
          </Item>
          <Item
            onClick={notificationsFunction}
            icon={<BellTwoTone />}
            key="2"
          >
            <StyledBadge count={0}>
              Notifications
            </StyledBadge>
          </Item>
          <Item
            onClick={Logout}
            icon={<LogoutOutlined />}
            key="3"
          >
            Logout
          </Item>
        </Menu>
      </Row>
    </Header>
  );
}

const StyledBadge = styled(Badge)`
  color: #aaa;
`;