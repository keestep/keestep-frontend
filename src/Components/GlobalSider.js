import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Layout, Menu, Avatar, Typography, Badge, notification } from "antd";
import { LogoutOutlined } from "@ant-design/icons";
import {
  FcHome,
  FcConferenceCall,
  FcCalendar,
  FcViewDetails,
  FcLibrary,
  FcDataSheet,
  FcLike,
  FcCustomerSupport,
  FcIdea,
  FcTemplate
} from 'react-icons/fc';
// assets
import thumbnail from '../Assets/Img/thumb-1.jpg';
// components
import { UserProfile } from '../Containers/UserProfile';
// api
import { getNotificationCount } from "../Api/Notifications";
import { Logout } from '../Api/AuthApi';

export const GlobalSider = ({ menuFunction, profile }) => {
  const { Item, SubMenu } = Menu;
  const { Link } = Typography;

  const [notificationCount, setNotificationCount] = useState(null);

  const fireApis = async () => {
    try {
      const res = await getNotificationCount();
      if (res && res.success) {
        setNotificationCount(res.data);
      }
    } catch (e) {
      notification.error({ message: e.message })
    }
  }

  useEffect(() => {
    fireApis();
  }, []);

  const menu = [
    {
      title: "Home", icon: <FcHome size={20} />, route: "/"
    },
    {
      title: "Users", icon: <FcConferenceCall size={20} />,
      levels: [
        { title: "Students", route: "/students" },
        { title: "Faculty", route: "/faculty" },
        { title: "Administrators", route: "/admin" },
      ]
    },
    {
      title: "College Setup", icon: <FcLibrary size={20} />,
      levels: [
        { title: "College Profile", route: "/college-profile" },
        { title: "Subjects", route: "/subjects" },
        { title: "Books", route: "/books" },
        { title: "Courses", route: "/courses" },
        { title: "Branches", route: "/branches" },
      ]
    },
    { title: "Attendance", icon: <FcViewDetails size={20} />, route: "/attendance" },
    { title: "Timetable", icon: <FcCalendar size={20} />, route: "/timetable" },
    { title: "Marks", icon: <FcDataSheet size={20} />, route: "/marks" },
    { title: "Social", icon: <FcLike size={20} />, route: "/social-media" },
    { title: "Queries", icon: <FcCustomerSupport size={20} />, route: "/queries" },
    { title: "Notice Board", icon: <FcTemplate size={20} />, route: "/noticeboard" },
    { title: "Notifications", icon: <FcIdea size={20} />, route: "/notifications", count: notificationCount },
  ];

  return (
    <SiderComponent collapsible>
      <Logo>
        <BigAvatar src={profile ? profile.photo_url: thumbnail} />
        <Username type="warning" strong ellipsis>{profile.name}</Username>
        <Link
          code
          key="user-profile"
          href="user-profile"
          onClick={() => menuFunction(<UserProfile />)}
        >
          Edit Profile
        </Link>
      </Logo>

      <Menu theme="dark" mode="inline">
        {menu.filter(each => !!each.title).map(({ title, icon, levels, route, count }) => (
          <>
            {levels
              ? (
                <SubMenu key={title} icon={icon} title={title}>
                  {levels.map(({ title, route }) => (
                    <Item key={title} title={title} >
                      <a href={route} >{title}</a>
                    </Item>
                  ))}
                </SubMenu>
              )
              : (
                <Item key={title} icon={icon} >
                  <a href={route} >
                    <StyledBadge count={count}>
                      {title}
                    </StyledBadge>
                  </a>
                </Item>
              )}
          </>
        ))}
        <Item
          key="logout"
          onClick={Logout}
          icon={<LogoutOutlined />}
        >
          Logout
        </Item>
      </Menu>
    </SiderComponent>
  );
}

const Logo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  height: 150px;
  color: white;
  margin: 30px 0;
`;

const SiderComponent = styled(Layout.Sider)`
  height: 100vh;
  min-width: 180px;
  position: fixed;
  overflow: hidden;
`;

const BigAvatar = styled(Avatar)`
  width: 70px;
  height: 70px;
`;

const Username = styled(Typography.Text)`
  font-size: 20px;
  width: 60%;
  text-align: center;
`;

const StyledBadge = styled(Badge)`
  color: #bbb;

  &:hover {
    color: white;
  }
`;