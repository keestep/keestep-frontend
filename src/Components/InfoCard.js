import React from "react";
import styled from "styled-components";
import { Card, Button, Avatar, Badge } from "antd";

export const InfoCard = ({
  loading,
  color,
  setFormStatus,
  children,
  title,
  description,
  actionTitle,
  count,
  icon
}) => {
  const avatarStyle = { backgroundColor: "#0984E3" };
  const { Meta } = Card;

  return (
    <Card
      loading={loading}
      style={{ width: 250, marginTop: 16, backgroundColor: color }}
      actions={[
        <WideButton type="primary" onClick={() => setFormStatus(true)}>
          {actionTitle}
        </WideButton>,
      ]}
    >
      <Meta
        avatar={
          <Badge count={count}>
            <Avatar icon={icon} style={avatarStyle}>C</Avatar>
          </Badge>
        }
        title={<b>{title}</b>}
        description={`${count} ${description}`}
      />
      {children}
    </Card>
  );
};


const WideButton = styled(Button)`
  width: 90%;
  border-radius: 10px;
`;