import React from 'react';

export const Spacer = ({ size, horizontal }) => {
  return (
    <>
      {horizontal
        ? <div style={{ width: size }} />
        : <div style={{ height: size }} />
      }
    </>
  );
}
