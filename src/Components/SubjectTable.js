import React, { useState } from "react";
import styled from 'styled-components';
import { Table, Typography, Popconfirm } from "antd";
import { DeleteFilled } from "@ant-design/icons";

import { SubjectForm } from "../Containers/HomeDashboard/components/SubjectForm";

const { Title, Link } = Typography;

export const SubjectTable = ({ isAdmin }) => {

  const [subjectFormVisible, setSubjectFormVisible] = useState(false);

  const dataSource = [
    {
      key: "1",
      subject_name: "Mathematics-I",
      course_code: "BTECH",
      subject_code: "M1",
      year: 2,
      semester: 1,
      branch_name: "ECE",
      books: [
        { title: "Mathematics-1", author: "Tannenbaum" }
      ]
    },
    {
      key: "2",
      subject_name: "Applied Physics",
      course_code: "BTECH",
      subject_code: "AP",
      year: 2,
      semester: 1,
      branch_name: "ECE",
      books: [
        { title: "Applied Physics", author: "Charlie" }
      ],
    },
    {
      key: "3",
      subject_name: "Electronic Devices and Circuits",
      course_code: "BTECH",
      subject_code: "EDC",
      year: 2,
      semester: 1,
      branch_name: "ECE",
      books: [
        { title: "Electronic Devices and Circuits", author: "K.S.Agarwal" }
      ]
    },
  ];

  const columns = [
    {
      title: "Subject name",
      dataIndex: "subject_name",
      key: "subject_name",
      render: (text) => (
        <Link onClick={() => setSubjectFormVisible(true)}>{text}</Link>
      )
    },
    {
      title: "Course Code",
      dataIndex: "course_code",
      key: "course_code",
      filters: [{ text: 'B.Tech', value: 'BTECH' }]
    },
    {
      title: "Subject Code",
      dataIndex: "subject_code",
      key: "subject_code",
    },
    {
      title: "Year",
      dataIndex: "year",
      key: "year",
    },
    {
      title: "Semester",
      dataIndex: "semester",
      key: "semester",
    },
    {
      title: "Branch Name",
      dataIndex: "branch_name",
      key: "branch",
    }, 
  ];

  const columnsWithActions = [
    ...columns,
    {
      title: "Take action",
      render: (_, record) => (
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Popconfirm title="Sure to delete?" onConfirm={() => null}>
            <DeleteFilled />
          </Popconfirm>
        </div>
      )
    }
  ]

  const handleChange = (p, filters, s) => console.log(filters)

  return (
    <Container>
      <Title level={3}>Subjects</Title>
      <Table
        dataSource={dataSource}
        columns={isAdmin ? columnsWithActions : columns}
        onChange={handleChange}
      />
      <SubjectForm
        id={2}
        isAdmin={isAdmin}
        visible={subjectFormVisible}
        onClose={() => setSubjectFormVisible(false)}
      />
    </Container>
  );
};


const Container = styled.div`
  width: 100%;
`;