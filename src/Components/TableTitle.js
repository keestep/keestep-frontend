import React from 'react';
import { Button, Row, Typography, Badge } from "antd";

export const TableTitle = ({ title, btnTitle, btnAction, totalRecords, showAddCourse }) => {

  const { Title } = Typography;

  return (
    <Row justify="space-between">
      <Badge count={totalRecords}>
        <Title level={3}>{title}</Title>
      </Badge>
      {showAddCourse
      && (
        <Button type="primary" onClick={btnAction}>
          {btnTitle}
        </Button>
      )}
    </Row>
  );
}
