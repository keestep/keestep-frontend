import React from 'react';
import styled from "styled-components";
import { Row, Typography, Switch } from "antd";

export const ToggleEdit = ({ status, action }) => {

  return (
    <StyledRow justify="end">
      <ToggleText type="secondary">Unlock edit mode</ToggleText>
      <Switch
        checked={status}
        onChange={() => action(!status)}
      />
    </StyledRow>
  );
}

const ToggleText = styled(Typography.Text)`
  margin-right: 15px;
`;

const StyledRow = styled(Row)`
  margin-bottom: 15px; 
`;