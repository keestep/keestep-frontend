import React from 'react'
import { Result, Button } from 'antd';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const UnAuthorized = () => {
  return (
    <Container>
      <Result
        status="403"
        title="403"
        subTitle="Sorry, you are unauthorized to access this."
        extra={<Link to="/"><Button type="primary">Back Home</Button></Link>}
      />
    </Container>
  )
}

const Container = styled.div`
  width: 100%;
  height: 100vh;
  background-color: ${({ theme }) => theme.primary};
  display: flex;
  flex-direction: column;
  justify-content: center;
`;