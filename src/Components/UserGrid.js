import React, { useEffect, useState } from "react";
import styled from 'styled-components';
import {
  Typography,
  Alert,
  Row,
  Col,
  Card,
  Badge,
  Dropdown,
  Menu,
  Modal,
  notification,
} from "antd";
import {
  EyeOutlined,
  EditOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { FaEllipsisV } from "react-icons/fa";
// assets
import thumbnail from "../Assets/Img/thumb-1.jpg";
// components
import { StudentDetail } from "../Containers/Students/components/StudentDetail";
// apis
import { updateAdmin } from "../Api/Admin";
import { updateFaculty } from "../Api/Faculty";
import { updateStudent } from "../Api/Student";
// utils
import { USER_ROLES } from "../Utility/constant";
import Avatar from "antd/lib/avatar/avatar";

const { Title, Text } = Typography;

export const UserGrid = ({ data, profile, changed, loading }) => {

  const updateFunctionMap = {
    admin: updateAdmin,
    faculty: updateFaculty,
    student: updateStudent
  }

  const [visible, setVisible] = useState(false);
  const [editMode, setEditMode] = useState(true);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [details, setDetails] = useState(profile);

  useEffect(() => {
    setDetails(profile);
  }, [profile]);

  const deleteUser = async (id, role) => {
    if(details._id === id) {
      notification.warn({ message: "You cannot delete yourself when logged in" })
    } else {
      const res = await updateFunctionMap[role](id, { deleted: true });
      if(res.success) {
        notification.success({ message: "Admin has been deleted" });
        changed(0);
      }
    }
  }

  const renderDeleteModal = (id, role) =>(
    <Modal
      visible={deleteModalOpen}
      onOk={() => deleteUser(id, role)}
      okText="Delete"
      onCancel={() => setDeleteModalOpen(false)}
    >
      <Text>Are you sure you want to delete this user?</Text>
      <Alert type="warning" message="This action is permanent and cannot be restored" />
    </Modal>
  )

  return (
    <Container>
      <StyledRow gutter={[24, 24]}>
        {data.map((user) => {

          let badgeValue = "";
          let subTitle = "";

          if (user.role === USER_ROLES.admin) {
            var { name, email, photo_url } = user;
            subTitle = email;
          }

          if (user.role === USER_ROLES.faculty) {
            var { name, email, photo_url, branch_name, subjects, is_HOD } = user;
            subTitle = `${branch_name}, ${subjects[0]}`;
            badgeValue = is_HOD ? "HOD": "";
          }
          
          if (user.role === USER_ROLES.student) {
            var { _id, name, course, photo_url, year, branch, section, class_representative } = user;
            subTitle = `${course}, ${branch}-${year}${section}`;
            badgeValue = class_representative ? "CR": "";
          }

          const handleView = () => {
            setDetails(user);
            setEditMode(true);
            setVisible(true);
          };
          const handleEdit = () => {
            setDetails(user);
            setEditMode(false);
            setVisible(true);
          };
          const handleDelete = () => {
            setDetails(user);
            setDeleteModalOpen(true)
          };

          const menu = (
            <Menu>
              <Menu.Item icon={<EyeOutlined />} onClick={handleView}>
                View
              </Menu.Item>
              <Menu.Item icon={<EditOutlined />} onClick={handleEdit}>
                Edit
              </Menu.Item>
              <Menu.Item icon={<DeleteOutlined />} onClick={handleDelete}>
                Delete
              </Menu.Item>
            </Menu>
          );

          return (
            <Col span={4}>
              <Badge count={badgeValue}>
                <UserCard loading={loading}>
                  <Dropdown overlay={menu}>
                    <OptionsIcon />
                  </Dropdown>
                  <UserInfo>
                    <Avatar src={photo_url || thumbnail} size={60} />
                    <Username ellipsis level={5}>{name}</Username>
                    <p>{subTitle}</p>
                  </UserInfo>
                </UserCard>
              </Badge>
            </Col>
          )}
        )}
        {details && renderDeleteModal(details._id, details.role)}
        {details
        &&
          <StudentDetail
            data={details}
            visible={visible}
            edit={editMode}
            onClose={() => setVisible(false)}
          />
        }
      </StyledRow>
    </Container>
  );
};


const Container = styled.div`
  width: 100%;
`;

const StyledRow = styled(Row)`
  margin: 50px 0;
`;

const Username = styled(Title)`
  width: 100%;
  margin-top: 10px;
`;

const UserCard = styled(Card)`
  border-radius: 15px;
  width: 180px;
`;

const UserInfo = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  text-align: center;
`;

const OptionsIcon = styled(FaEllipsisV)`
  position: absolute;
  right: 15px;
  top: 15px;
`;