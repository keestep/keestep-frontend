import React from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { PageHeader } from 'antd';
import ComingSoon from '../../Components/ComingSoon';

export default function Attendance () {
  return (
    <Container>
      <Helmet>
        <title>Attendance</title>
        <meta name="description" content="Mark, track and visualize attendance" />
      </Helmet>
      <PageHeader title="Attendance" />
      <ComingSoon />
    </Container>
  );
}

const Container = styled.div`
  height: 100vh;
`;