import React, { useState } from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { PageHeader, Empty, Button } from 'antd';

// components
import { FilterStrip } from '../../Components/FilterStrip';
// constants
import { SORT_OPTIONS } from '../../Utility/constant';
// images
import BookShelve from '../../Assets/Img/Bookshelves.svg';

export const Books = () => {

  const [sort, setSort] = useState(1);
  const [books, setBooks] = useState([]);

  return (
    <Container>
      <Helmet>
        <title>Books</title>
        <meta name="description" content="Manage your digital library" />
      </Helmet>
      <PageHeader title="Books" />
      <FilterStrip
        filters={[
          {
            label: 'Sort: ',
            default: SORT_OPTIONS[0].value,
            action: setSort,
            options: SORT_OPTIONS,
          }
        ]}
      />
      {books.length === 0
      && (
        <Center>
          <Empty
            image={BookShelve}
            imageStyle={{ height: 200 }}
            description="Add books to your library"
            >
            <Button type="primary">Add Book</Button>
          </Empty>
        </Center>
      )
      }
    </Container>
  )
}

const Container = styled.div`
  height: 100vh;
`;

const Center = styled.div`
  height: 60vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;