import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import {
  PageHeader, Descriptions, Row, 
  Typography, Col, message, Upload,
  Button, Spin, Space
} from 'antd';
import { FaUpload } from "react-icons/fa";

// apis
import { getCollege } from '../../Api/College';
//assets
import thumbnail from '../../Assets/Img/thumb-1.jpg';
// components
import { CollegeForm } from './components/CollegeForm';
// utils
import { field } from '../../Utility/fieldMap';

export const CollegeProfile = () => {

  const { Item } = Descriptions;
  const { Text } = Typography;

  const [college, setCollege] = useState('');
  const [collegeId, setCollegeId] = useState(null);
  const [photo, setPhoto] = useState(null);
  const [editMode, setEditMode] = useState(false);
  const [loading, setLoading] = useState(false);

  async function fireCollegeApi() {
    setLoading(true);
    const res = await getCollege();

    if (res.success) {
      const { _id, college_name, photo_url, website, phone, address, updated_at, updated_by } = res.data;
      setCollege({
        college_name, website,
        phone, address,
        updated_at: new Date(updated_at).toUTCString(),
        updated_by
      });
      setPhoto(photo_url);
      setCollegeId(_id);
    } else {
      message.error(res.message)
    }
    setLoading(false);
  }

  useEffect(() => {
    fireCollegeApi()
  }, [editMode]);

  const uploadProps = {
    name: 'file',
    onChange(info) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  return (
    <Container>
      <Helmet>
        <title>College Profile</title>
        <meta name="description" content="View information about college" />
      </Helmet>
      <PageHeader title="User Profile" />
      {loading
        ? (
          <SpinnerContainer justify="center">
            <Spin />
          </SpinnerContainer>
        )
        : (
          <Row justify="space-around" style={{ marginTop: '80px' }}>
            <Col span={8}>
              <ProfilePicContainer>
                <ProfilePic src={photo || thumbnail} />
                <Upload {...uploadProps} >
                  <Button icon={<UploadIcon />}>
                    Upload new picture
                  </Button>
                </Upload>
              </ProfilePicContainer>
            </Col>
            <Col span={16}>
              <StyledRow justify="end">
                <Button type="primary" onClick={() => setEditMode(!editMode)}>
                  {editMode ? "Back" : "Edit details"}
                </Button>
              </StyledRow>
              {editMode
                ? (
                  <CollegeForm
                    id={collegeId}
                    data={college}
                    setEditMode={setEditMode}
                    setCollegeData={setCollege}
                  />
                )
                : (
                  <Descriptions bordered>
                    {Object.keys(college).map(key => (
                      <Item
                        span={10}
                        label={(
                          <Space>
                            {field[key].icon}
                            <Text>{field[key].name}</Text>
                          </Space>
                        )}
                      >
                        {college[key]}
                      </Item>
                    ))}
                  </Descriptions>
                )
              }
            </Col>
          </Row>
        )
      }
    </Container>
  );
}

const Container = styled.div`
  height: 100vh;
  width: 100%;
`;

const UploadIcon = styled(FaUpload)`
  margin-right: 10px;
`;

const ProfilePic = styled.img`
  border-radius: 50%;
  width: 200px;
  margin-bottom: 30px;
`;

const ProfilePicContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const StyledRow = styled(Row)`
  margin-bottom: 20px;
  width: 70%;
`;

const SpinnerContainer = styled(Row)`
  height: 400px;
`;