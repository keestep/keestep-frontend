import React, { useState } from 'react';
import styled from "styled-components";
import { message, notification } from "antd";

import { FormComponent } from "../../../Components/Form";
import { updateCollege } from "../../../Api/College";

export const CollegeForm = ({ id, data, setEditMode, setCollegeData }) => {

  const [loading, setLoading] = useState(false);

  const onFinish = async ({ role, ...values }) => {
    setLoading(true);
    const res = await updateCollege(id, values);
    if(res.success) {
      notification.success({ message: "Profile updated successfully" });
      const {
        college_name, website, phone,
        address, updated_at, updated_by
      } = res.data;
      setCollegeData({
        name: college_name, website, phone, address,
        updated_at: new Date(updated_at).toUTCString(),
        updated_by
      });
    }
    setLoading(false);
    setEditMode(false);
  };

  const onFinishFailed = (errorInfo) => {
    message.error(errorInfo);
  };

  return (
    <Container>
      <FormComponent
        name="Update User details"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        btnLoading={loading}
        submitButtonTitle="Save"
        formItems={[
          {
            initialValue: data && data.college_name,
            label: "College Name",
            name: "college_name",
            rules: [],
            type: {
              name: 'input',
              props: {  }
            }
          },
          {
            initialValue: data && data.website,
            label: "Website",
            name: "website",
            rules: [],
            type: {
              name: 'input',
              props: {  }
            }
          },
          {
            initialValue: data && data.phone,
            label: "Phone Number",
            name: "phone",
            rules: [],
            type: {
              name: 'input',
              props: { type: 'tel' }
            }
          },
          {
            initialValue: data && data.address,
            label: "Address",
            name: "address",
            rules: [],
            type: {
              name: 'input',
              props: {  }
            }
          },
        ]}
      />
    </Container>
  );
}

const Container = styled.div`
  width: 70%;
`;