import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import {
  PageHeader,
  Empty,
  Button,
  Space,
  Popconfirm,
  Table,
  notification,
  Row,
  Affix,
} from 'antd';
import { DeleteFilled } from '@ant-design/icons';
import { FaFolderPlus } from 'react-icons/fa';

// components
import { FilterStrip } from '../../Components/FilterStrip';
import { CourseForm } from './components';
// constants
import { SORT_OPTIONS } from '../../Utility/constant';
// images
import Graduation from '../../Assets/Img/Graduation.svg';
// apis
import { getCourses, updateCourse } from '../../Api/Course';
import { field } from '../../Utility/fieldMap';

export const Courses = () => {

  const [sort, setSort] = useState(1);
  const [courses, setCourses] = useState([]);
  const [formVisible, setFormVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [makeChange, setMakeChange] = useState(0);

  const fireApis = async () => {
    setLoading(true);
    try {
      const { success, data } = await getCourses();
      if (success) {
        setCourses(data);
      } else {
        notification.error({ message: "Error fetching courses" })
      }
    } catch (e) {
      notification.error({ message: e.message })
    }
    setLoading(false);
  }

  const deleteCourse = async (id) => {
    setLoading(true);
    try {
      const { success, data } = await updateCourse(id, { deleted: true });
      if (success) {
        setCourses(data);
      } else {
        notification.error({ message: "Error deleting course" })
      }
    } catch (e) {
      notification.error({ message: e.message })
    }
    setLoading(false);
  }

  useEffect(() => {
    fireApis();
  }, [makeChange]);

  return (
    <Container>
      <Helmet>
        <title>Courses</title>
        <meta name="description" content="Manage your courses" />
      </Helmet>
      <PageHeader title="Courses" />
      {courses.length !== 0
        && (
          <AddButtonContainer justify="end">
            <Button type="primary" onClick={() => setFormVisible(true)}>
              <Space><FaFolderPlus />Add Course</Space>
            </Button>
          </AddButtonContainer>
        )}
      {!loading && courses.length === 0
        ? (
          <Center>
            <Empty
              image={Graduation}
              imageStyle={{ height: 300 }}
              description="Add courses from here"
            >
              <Button type="primary" onClick={() => setFormVisible(true)}>
                <Space><FaFolderPlus />Add Course</Space>
              </Button>
            </Empty>
          </Center>
        )
        : (
          <>

            <Table
              dataSource={courses}
              loading={loading}
              columns={[
                {
                  key: "1",
                  title: field.course_name.name,
                  dataIndex: "course_name",
                  filters: courses ? courses.map(({ course_name }) => ({
                    text: course_name,
                    value: course_name
                  })) : [],
                  onFilter: (value, { course_name }) => course_name.includes(value),
                  sorter: {
                    compare: (a, b) => a.course_name > b.course_name
                  },
                },
                {
                  key: "2",
                  title: field.course_code.name,
                  dataIndex: "course_code",
                },
                {
                  key: "3",
                  title: field.duration.name,
                  dataIndex: "duration",
                  sorter: true
                },
                {
                  key: "4",
                  title: field.credits.name,
                  dataIndex: "credits",
                },
                {
                  key: "5",
                  title: field.year_of_intake.name,
                  dataIndex: "year_of_intake",
                  sorter: {
                    compare: (a, b) => a.year_of_intake - b.year_of_intake,
                    multiple: 1
                  },
                },
                {
                  key: "6",
                  title: field.year_of_passout.name,
                  dataIndex: "year_of_passout",
                  sorter: {
                    compare: (a, b) => a.year_of_intake - b.year_of_intake,
                    multiple: 1
                  },
                },
                {
                  title: "Take action",
                  render: (_, record) => (
                    <Row justify="center">
                      <Popconfirm
                        title="Are you sure you want to delete this course?"
                        onConfirm={() => deleteCourse(record._id)}
                      >
                        <Space>
                          Delete
                          <DeleteFilled />
                        </Space>
                      </Popconfirm>
                    </Row>
                  )
                }
              ]}
            />
          </>
        )}
      <CourseForm
        visible={formVisible}
        onClose={() => setFormVisible(false)}
        makeChange={setMakeChange}
      />
    </Container>
  )
}

const Container = styled.div`
  height: 100vh;
  padding: 22px;
`;

const Center = styled.div`
  height: 60vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const AddButtonContainer = styled(Row)`
  height: 80px;
`;