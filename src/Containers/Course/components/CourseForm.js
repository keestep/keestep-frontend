import React, { useState } from 'react';
import { Drawer, notification } from 'antd';

// components
import { FormComponent } from '../../../Components/Form';
import { field } from '../../../Utility/fieldMap';
// apis
import { createCourse } from '../../../Api/Course';

export const CourseForm = ({ visible, onClose, makeChange }) => {

  const [loading, setLoading] = useState(false);

  const handleFinish = async ({ duration, credits, year_of_intake, year_of_passout, ...input }) => {
    setLoading(true);
    try {
      const { success } = await createCourse({
        duration: parseInt(duration),
        credits: parseInt(credits),
        year_of_intake: parseInt(year_of_intake),
        year_of_passout: parseInt(year_of_passout),
        ...input
      });
      if(success) {
        notification.success({ message: "Course added successfully" });
        makeChange(1);
      } else {
        notification.error({ message: "Unknown error while adding course" })
      }
    } catch (e) {
      notification.error({ message: e.message })
    }
    setLoading(false);
  };

  const handleFailure = ({ errorFields }) => {
    errorFields.map(e => notification.error({ message: e.errors[0], duration: 2 }))
  };

  return (
    <Drawer
      closable
      visible={visible}
      onClose={onClose}
      placement="right"
      width={450}
      title="Add Course"
    >
      <FormComponent
        name='Add Course'
        onFinish={handleFinish}
        onFinishFailed={handleFailure}
        btnLoading={loading}
        submitButtonTitle='Submit'
        formItems={[
          {
            initialValue: '',
            label: field.course_name.name,
            name: "course_name",
            rules: [{ required: true }],
            type: {
              name: 'input',
              props: {
                prefix: field.course_name.icon
              }
            }
          },
          {
            initialValue: '',
            label: field.course_code.name,
            name: "course_code",
            rules: [{ required: true }],
            type: {
              name: 'input',
              props: {
                prefix: field.course_code.icon
              }
            }
          },
          {
            initialValue: '',
            label: field.duration.name,
            name: "duration",
            rules: [{ required: true }],
            type: {
              name: 'number',
              props: {
                min: 1,
                prefix: field.duration.icon
              }
            }
          },
          {
            initialValue: '',
            label: field.credits.name,
            name: "credits",
            rules: [{ required: true }],
            type: {
              name: 'number',
              props: {
                min: 0,
                prefix: field.credits.icon
              }
            }
          },
          {
            initialValue: '',
            label: field.year_of_intake.name,
            name: "year_of_intake",
            rules: [{ required: true }],
            type: {
              name: 'number',
              props: {
                prefix: field.year_of_intake.icon,
                min: 2000
              }
            }
          },
          {
            initialValue: '',
            label: field.year_of_passout.name,
            name: "year_of_passout",
            rules: [{ required: true }],
            type: {
              name: 'number',
              props: {
                prefix: field.year_of_passout.icon,
                min: 2000
              }
            }
          },
        ]}
      />
    </Drawer>
  )
}
