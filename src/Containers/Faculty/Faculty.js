import React, { useState, useEffect } from "react";
import { Helmet } from 'react-helmet';
import styled from "styled-components";
import {
  PageHeader, Statistic,
  Row, Col, Card, message,
  Button, Popconfirm, Space
} from "antd";
import {
  FaUserPlus
} from 'react-icons/fa';

// apis
import { getStudents } from "../../Api/Student";
// components
import { UserGrid } from "../../Components/UserGrid";
import { StudentEntryForm } from "./components/FacultyForm";

export const Faculty = ({ profile }) => {

  const [students, setStudents] = useState([]);

  const [loading, setLoading] = useState(false);
  const [makeChange, setMakeChange] = useState(false);
  const [entryFormVisible, setEntryFormVisible] = useState(false);

  const fireApis = async () => {
    
    const studentsRes = await getStudents();

    if(studentsRes.success) {
      setStudents(studentsRes.data)
    } else {
      message.error(studentsRes.message);
    }
  };

  useEffect(() => {
    setLoading(true);
    fireApis();
    setLoading(false);
  },[makeChange]);

  return (
    <div style={{ height: "100vh" }}>
      <Helmet>
        <title>Students</title>
        <meta name="description" content="Organize your students data" />
      </Helmet>
      <PageHeader title="Students" />
      <Container>
        <Row justify="space-between">
          <Col span={4}>
            <Card>
              <Statistic loading={loading} title="Total Students" value={students.length} />
            </Card>
          </Col>
          <Col span={18}>
          <Row justify="end">
            <Popconfirm
              okType="primary"
              title="How do you want to add students?"
              okText="Single"
              cancelText="Multiple at once"
              onConfirm={() => setEntryFormVisible(true)}
              onCancel={() => null}
              placement="right"
            >
              <Button type="primary">
                <Space><FaUserPlus />Add Students</Space>
              </Button>
            </Popconfirm>
          </Row>
          </Col>
        </Row>
        <div style={{ height: 50 }} />
        <UserGrid
          data={students}
          profile={profile}
          changed={setMakeChange}
          loading={loading}
        />
        <StudentEntryForm
          visible={entryFormVisible}
          onClose={() => setEntryFormVisible(false)}
          makeChange={makeChange}
        />
      </Container>
    </div>
  );
};

const Container = styled.div`
  padding: 0 22px;
`;
