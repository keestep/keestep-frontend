import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Descriptions, Modal, Avatar, Spin, notification } from 'antd';

// components
import { FormComponent } from '../../../Components/Form';
// apis
import { updateFaculty } from '../../../Api/Faculty';
// utils
import { BLOOD_GROUPS, GENDER_TYPES } from '../../../Utility/constant';
import { field } from '../../../Utility/fieldMap';
import { lightTheme } from '../../../Utility/theme.config';
// assets
import thumbnail from '../../../Assets/Img/thumb-1.jpg';

export const FacultyDetail = ({ onClose, visible, data, edit }) => {

  const { Item } = Descriptions;

  const [editMode, setEditMode] = useState(true);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setEditMode(edit);
  }, [edit]);

  const labelMap = [
    'name',
    'age',
    'reg_id',
    'interests',
    'class_representative',
    'email',
    'course',
    'branch',
    'year',
    'semester',
    'section',
    'address',
    'blood_group',
    'gender',
    'year_of_intake',
    'year_of_passout',
    'created_at',
    'created_by',
  ]

  const handleUpdate = async (values) => {
    setLoading(true);
    try {
      const { success, message } = await updateFaculty(data._id, values);
      if (success) {
        notification.success({ message: "Faculty details updated successfully" });
        setEditMode(true);
      } else {
        notification.error({ message });
      }
    } catch (e) {
      notification.error({ message: e.message});
    }
    setLoading(false);
  }

  const handleFailure = (errorInfo) => {
    notification.error({ message: errorInfo});
  };

  return (
    <BorderedModal
      closable
      width="60%"
      onCancel={onClose}
      cancelButtonProps={{ type: 'text' }}
      okText={editMode? "Edit": "Back"}
      onOk={() => setEditMode(!editMode)}
      visible={visible}
      title={editMode ? "Faculty Information": "Update Faculty info"}
    >
      <PlacedAvatar src={data.photo_url || thumbnail} size={65} />
      {loading
      ? <Spin spinning style={{height: '90%'}} />
      : (
        <>
        {editMode
        ? (
          <Descriptions size="middle" bordered>
            {data && labelMap.map(key => {
              let val = data[key];
              if (val && key === "interests") {
                val = data[key].join(',');
              } else if (key === "created_at") {
                val = new Date(data[key]).toUTCString();
              } else if (key === "class_representative") {
                val = data[key] ? "Yes": "No";
              }
              return (
                <Item
                  labelStyle={{ backgroundColor: lightTheme.background }}
                  span={2}
                  label={field[key]}
                  >
                  {val}
                </Item>
              )
            })}
          </Descriptions>
        )
        : (
          <FormComponent
            name="Update Faculty"
            onFinish={handleUpdate}
            onFinishFailed={handleFailure}
            btnLoading={loading}
            submitButtonTitle="Update Faculty"
            formItems={[
              {
                initialValue: data ? data.name : "",
                label: labelMap.name,
                name: "name",
                type: {
                  name: 'input',
                  props: {
                    prefix: field.name.icon
                  }
                }
              },
              {
                initialValue: data ? data.email : "",
                label: labelMap.email,
                name: "email",
                type: {
                  name: 'input',
                  props: { 
                    type: 'email',
                    prefix: field.email.icon
                  }
                }
              },
              {
                initialValue: data ? data.class_representative : "",
                label: labelMap.class_representative,
                name: "class_representative",
                type: {
                  name: 'checkbox',
                  props: { 
                  }
                }
              },
              {
                initialValue: data ? data.address : "",
                label: labelMap.address,
                name: "address",
                type: {
                  name: 'textarea',
                  props: { 
                    prefix: field.address.icon
                  }
                }
              },
              {
                initialValue: data ? data.age : "",
                label: labelMap.age,
                name: "age",
                type: {
                  name: 'input',
                  props: {
                    type: 'number',
                    props: { 
                      prefix: field.email.icon
                    }
                  }
                }
              },
              {
                initialValue: data ? data.blood_group : "",
                label: labelMap.blood_group,
                name: "blood_group",
                type: {
                  name: 'select',
                  props: {
                    options: BLOOD_GROUPS,
                    props: { 
                      prefix: field.blood_group.icon
                    }
                  }
                }
              },
              {
                initialValue: data ? data.gender : "",
                label: labelMap.gender,
                name: "gender",
                type: {
                  name: 'select',
                  props: {
                    options: GENDER_TYPES,
                    props: { 
                      type: 'email',
                      prefix: field.email.icon
                    }
                  }
                }
              },
            ]}
          />
        )}
        </>
      )}
    </BorderedModal>
  )
}

const BorderedModal = styled(Modal)`
  overflow: hidden;
  z-index: -1;
`;

const PlacedAvatar = styled(Avatar)`
  position: absolute;
  left: 45%;
  top: 25px;
  display: block;
`;