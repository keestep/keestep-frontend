import React from "react";
import { Helmet } from 'react-helmet';
import { Switch } from "react-router-dom";
import styled from "styled-components";
import { Layout } from "antd";

// Containers
import { HomeDashboard } from "../HomeDashboard";
import { UserProfile } from '../UserProfile';
import { Students } from "../Students";
import { Faculty } from "../Faculty";
import { Attendance } from "../Attendance";
import { Notifications } from "../Notifications";
import { CollegeProfile } from "../CollegeProfile";
import { Books } from "../Books";
import { Courses } from "../Course";
import { SocialMedia } from "../Social";
import { Marks } from "../Marks";
import { Queries } from "../Queries";

// Components
import { GlobalSider } from "../../Components/GlobalSider";
import { PrivateRoute } from "../../Components/PrivateRoute";
// APIs
import { getCurrentUser } from "../../Api/AuthApi";


const Home = () => {

  const profile = getCurrentUser(); 
  const user = profile && profile.data;

  return (
    <Layout>
      <Helmet>
        <title>Home</title>
        <meta name="description" content="Organize your students data" />
      </Helmet>
      <GlobalSider
        profile={user}
      />
      <StyledLayout>
        <MenuContent>
            <Switch>
              <PrivateRoute exact path="/" component={HomeDashboard} profile={user} />
              <PrivateRoute exact path="/user-profile" component={UserProfile} />
              <PrivateRoute exact path="/students" component={Students} />
              <PrivateRoute exact path="/faculty" component={Faculty} />
              <PrivateRoute exact path="/books" component={Books} />
              <PrivateRoute exact path="/courses" component={Courses} />
              <PrivateRoute exact path="/attendance" component={Attendance} />
              <PrivateRoute exact path="/marks" component={Marks} />
              <PrivateRoute exact path="/social-media" component={SocialMedia} />
              <PrivateRoute exact path="/queries" component={Queries} />
              <PrivateRoute exact path="/college-profile" component={CollegeProfile} />
              <PrivateRoute exact path="/notifications" component={Notifications} />
            </Switch>
        </MenuContent>
      </StyledLayout>
    </Layout>
  ) 
};

export default Home;

const StyledLayout = styled(Layout)`
  overflow: hidden;
  margin-left: 180px;
`;

const MenuContent = styled(Layout.Content)`
  margin: 24px 16px 0;
  width: 100%;
  height: 100%;
  padding-right: 17px;
  box-sizing: content-box;
`;