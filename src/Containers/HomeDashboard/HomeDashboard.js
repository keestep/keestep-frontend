import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Helmet } from 'react-helmet';
import TimeDiff from 'js-time-diff';
import ruled from 'ruled'
import {
  List, Skeleton, PageHeader,
  Typography, Card, notification, Button
} from 'antd';

// Container Components
import { Notifications } from '../Notifications'
// Components

// APIs
import { getNotifications } from "../../Api/Notifications";
import { getNews } from "../../Api/User";
import { getAvatar } from "../../Utility/NotificationIconMap";
import { lightTheme } from "../../Utility/theme.config";

export const HomeDashboard = ({ profile, setOption }) => {
  const { Title, Text, Paragraph } = Typography;

  const [loading, setLoading] = useState(true);
  const [notifications, setNotifications] = useState(0);
  const [newsData, setNewsData] = useState([]);
  const isAdmin = profile && profile.isAdmin;
  const name = profile && profile.name;

  const fireApis = async () => {
    setLoading(true);
    try {
      const [notificationsRes, newsRes] = await Promise.allSettled([
        getNotifications({ limit: 5 }),
        getNews()
      ])
      setNotifications(notificationsRes.value.data);
      setNewsData(newsRes.value.data);
    } catch (e) {
      notification.error({ message: e.message })
    }
    setLoading(false);
  }

  useEffect(() => {
    fireApis();
  },[]);

  return (
    <>
      <Helmet>
        <title>Home</title>
        <meta name="description" content="Find the most important updates here" />
      </Helmet>
      <PageHeader title="Home" />
      <Container>
        <Title level={2}>
          <span style={{ color: "#bbb" }}>{'Welcome, '}</span>
          {name}
        </Title>
        {!!(notifications) &&
          <>
            <Title level={4}>
              {'Your latest notifications'}
            </Title>
            <a onClick={() => setOption(<Notifications />)}>
              See all notifications
            </a>
            <StyledList
              loading={loading}
              itemLayout="horizontal"
              dataSource={notifications}
              renderItem={({ _id, title, description, created_at }) => (
                <List.Item
                  key={_id}
                >
                  <Skeleton avatar title={false} loading={false} active>
                    <List.Item.Meta
                      avatar={getAvatar(title)}
                      title={title}
                      description={description}
                    />
                    <Text type="secondary" >
                      {TimeDiff(created_at, new Date())}
                    </Text>
                  </Skeleton>
                </List.Item>
              )}
            />
            <Title level={4} >Latest News Feed from JNTUH</Title>
            <Paragraph type="secondary">{`Last fetched at ${newsData && newsData.lastFetchedAt}`}</Paragraph>
            <NewsSection>
              {newsData && newsData.news && newsData.news.map(({ description, url }, _id) => (
                <NewsCard
                  loading={loading}
                  key={_id}
                >
                  <Paragraph style={{ fontWeight: 600, color: 'white' }} ellipsis={{ rows: 6 }} >{description}</Paragraph>
                  <Button type="ghost" style={{ color: 'whitesmoke' }} href={url} target="_blank">{'See Full Article'}</Button>
                </NewsCard>
              ))}
            </NewsSection>
          </>
        }
      </Container>
    </>
  );
};

const Container = styled.div`
  padding: 0 22px;
`;

const StyledList = styled(List)`
  margin: 20px 30px;
  transition: 0.3s;
`;

const NewsSection = styled.div`
  display: flex;
  min-width: 100%;
  height: 320px;
  overflow-x: auto;
  overflow-y: hidden;
  -ms-overflow-style: none;
  scrollbar-width: none;
`;

const NewsCard = styled(Card)`
  border-radius: 15px;
  background-color: ${lightTheme.superAdmin};
  height: 250px;
  margin: 0 20px;
  padding: 15px;
  background-image: ${ruled({ size: 8, horizontal: true, vertical: true })};
  background-size: 8px 8px;
  box-shadow: -2px 4px 23px 0px rgba(58,134,255,0.54);
  -webkit-box-shadow: -2px 4px 23px 0px rgba(58,134,255,0.54);
  -moz-box-shadow: -2px 4px 23px 0px rgba(58,134,255,0.54);
`;