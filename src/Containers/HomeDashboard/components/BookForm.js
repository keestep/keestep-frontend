import React, { useState, useEffect } from 'react';
import styled from "styled-components";
import { Drawer, Row, notification, message, Typography, Switch } from 'antd';

import { FormComponent } from "../../../Components/Form";
import { createBook, updateBook } from "../../../Api/Book";

export const BookForm = ({ data, onClose, visible, editMode, changed }) => {

  const [loading, setLoading] = useState(false);
  const [disableField, setDisableField] = useState(editMode);
  const [bookData, setBookData] = useState(data);

  const onFinish = async (values) => {
    setLoading(true);
    if(!data) {
      const res = await createBook(values);
      if(res.success) {
        notification.success({ message: "Book created successfully" });
        changed(1);
      }
    } else {
      const res = await updateBook(data._id, values);
      if(res.success) {
        notification.success({ message: "Book updated successfully" });
        changed(1);
      }
    }
    setLoading(false);
  };

  const onFinishFailed = (errorInfo) => {
    message.error(errorInfo);
  };

  useEffect(() => {
    if(data) {
      setDisableField(true);
      setBookData(data)
    }
  }, []);

  return (
    <Drawer
      closable
      width={450}
      placement="right"
      onClose={onClose}
      visible={visible}
      title={`${!!data ? 'Update' : 'Add an'} Book`}
    >
      {data
      && (
        <StyledRow justify="end">
          <ToggleText type="secondary">Toggle edit mode</ToggleText>
          <Switch checked={disableField} onChange={() => setDisableField(!disableField)} />
        </StyledRow>
      )}
      <FormComponent
        name={`${!!data ? 'Update' : 'Add'} Book`}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        btnLoading={loading}
        submitButtonTitle={`${!!data ? 'Update' : 'Add'} this book now`}
        formItems={[
          {
            initialValue: !!data && bookData.title || "",
            label: "Title",
            name: "title",
            rules: [{ required: true, message: 'Title name is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: !!data && bookData.author || "",
            label: "Author",
            name: "author",
            rules: [{ required: false }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: !!data && bookData.isbn || "",
            label: "ISBN",
            name: "isbn",
            rules: [{ required: false }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: !!data && bookData.subject_name || "",
            label: "Subject Name",
            name: "subject_name",
            rules: [{ required: true, message: 'Subject name is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: !!data && bookData.subejct_code || "",
            label: "Subject Code",
            name: "subejct_code",
            rules: [{ required: true, message: 'Subject Code is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: !!data && bookData.course_code || "",
            label: "Course Code",
            name: "course_code",
            rules: [{ required: true, message: 'Course code is required!' }],
            type: {
              name: 'input',
              props: { maxLength: 6, disabled: disableField }
            }
          },
          {
            initialValue: !!data && bookData.branch_code || "",
            label: "Branch Code",
            name: "branch_code",
            rules: [{ required: true, message: 'Branch code is required!' }],
            type: {
              name: 'input',
              props: { maxLength: 6, disabled: disableField }
            }
          },
        ]}
      />
    </Drawer>
  )
}

const ToggleText = styled(Typography.Text)`
  margin-right: 15px;
`;

const StyledRow = styled(Row)`
  margin-bottom: 15px; 
`;