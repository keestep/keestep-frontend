import React, { useState, useEffect } from 'react';
import { Drawer, Row, notification } from 'antd';
import { EditTwoTone } from '@ant-design/icons';

import { FormComponent } from "../../../Components/Form";

export const BranchForm = ({ id, onClose, visible }) => {
  const onFinish = (values) => {
    notification.info(JSON.stringify(values));
    setBranchData(values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  const [disableField, setDisableField] = useState(false);
  const [branchData, setBranchData] = useState({
    branch_name: "",
    course_name: "",
    branch_code: "",
    year: null,
    section: ""
  })

  useEffect(() => {
    if(id) {
      setDisableField(true);
      // Fire API for response and set state
      setBranchData({
        branch_name: "Electronics and Communication Engineering",
        course_name: "B.Tech R19",
        branch_code: "ECE",
        year: 4,
        section: 'A',
      })
    }
  }, [id]);

  return (
    <Drawer
      closable
      width={400}
      placement="right"
      onClose={onClose}
      visible={visible}
      title="Add a Branch"
    >
      {id
      && (
        <Row justify="end">
          <EditTwoTone onClick={() => setDisableField(!disableField)} />
        </Row>
      )}
      <FormComponent
        name={`${id ? 'Update' : 'Add'} Branch`}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        submitButtonTitle={`${id ? 'Update' : 'Add'} this branch now`}
        formItems={[
          {
            initialValue: branchData.branch_name,
            label: "Branch Name",
            name: "branch_name",
            rules: [{ required: true, message: 'Branch name is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: branchData.course_name,
            label: "Course Name",
            name: "course_name",
            rules: [{ required: true, message: 'Course Name is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: branchData.branch_code,
            label: "Branch Code",
            name: "branch_code",
            rules: [{ required: true, message: 'Branch code is required!' }],
            type: {
              name: 'input',
              props: { maxLength: 6, disabled: disableField }
            }
          },
          {
            initialValue: branchData.year,
            label: "Year",
            name: "year",
            rules: [{ required: true, message: 'Year is required!' }],
            type: {
              name: 'input',
              props: { type: 'number', disabled: disableField }
            }
          },
          {
            initialValue: branchData.section,
            label: "Section",
            name: "section",
            rules: [{ required: true, message: 'Section is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
        ]}
      />
    </Drawer>
  )
}