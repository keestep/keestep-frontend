import React, { useEffect, useState } from 'react';

import { Drawer, notification } from 'antd';

import { FormComponent } from '../../../Components/Form';
import { ToggleEdit } from '../../../Components/ToggleEdit';

export const CourseForm = ({
  data,
  isAdmin,
  editMode,
  onClose,
  visible,
  createAction,
  updateAction,
  setFormData
}) => {

  const [loading, setLoading] = useState(false);
  const [disableField, setDisableField] = useState(!editMode);

  const onFinish = async ({ duration, credits, year_of_intake, year_of_passout, ...values }) => {
    setLoading(true);
    const course = {
      duration: parseInt(duration),
      credits: parseInt(credits),
      year_of_intake: parseInt(year_of_intake),
      year_of_passout: parseInt(year_of_passout),
      ...values
    }
    if(!editMode) {
      console.log(values);
      const res = await createAction(course);
      if(res.success) {
        notification.success({ message: "Course created successfully" });
      }
      onClose();
      setFormData(res.data);
    } else {
      const res = await updateAction(data._id, course);
      if(res.success) {
        notification.success({ message: "Course updated successfully" });
      }
      onClose();
      setFormData(res.data);
    }
    setLoading(false);
  };

  const onFinishFailed = (errorInfo) => {
    notification.error({
      message: "Error validating form",
      description: errorInfo
    });
  };

  useEffect(() => {

  }, []);

  return (
    <Drawer
      closable
      placement="right"
      onClose={onClose}
      visible={visible}
      title="Add a Course"
      width={500}
    >
      {editMode && isAdmin && <ToggleEdit status={disableField} action={setDisableField} />}
      <FormComponent
        name={`${editMode ? 'Update' : 'Add'} course`}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        btnLoading={loading}
        btnDisabled={disableField}
        submitButtonTitle={`${editMode ? 'Update' : 'Add'} this course now`}
        formItems={[
          {
            initialValue: data ? data.course_name : "",
            label: "Course Name",
            name: "course_name",
            rules: [{ required: true, message: 'Course name is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: data ? data.duration: "",
            label: "Course Duration(years*)",
            name: "duration",
            rules: [{ required: true, message: 'Course duration is required!' }],
            type: {
              name: 'input',
              props: { type: 'number', min: 0, disabled: disableField }
            }
          },
          {
            initialValue: data ? data.course_code: "",
            label: "Course Code",
            name: "course_code",
            rules: [{ required: true, message: 'Course code is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: data ? data.year_of_intake: "",
            label: "Year of Intake",
            name: "year_of_intake",
            rules: [{ required: true, message: 'Year of Intake is required!' }],
            type: {
              name: 'input',
              props: { type: 'number', min: (new Date().getFullYear()-6), disabled: disableField }
            }
          },
          {
            initialValue: data ? data.year_of_passout: "",
            label: "Year of Passout",
            name: "year_of_passout",
            rules: [{ required: true, message: 'Year of Passout is required!' }],
            type: {
              name: 'input',
              props: { type: 'number', min: (new Date().getFullYear()-6), disabled: disableField }
            }
          },
          {
            initialValue: data ? data.credits: "",
            label: "Credits",
            name: "credits",
            rules: [{ required: true, message: 'Course credits is required!' }],
            type: {
              name: 'input',
              props: { type: 'number', min: 0, disabled: disableField }
            }
          },
        ]}
      />
    </Drawer>
  )
}