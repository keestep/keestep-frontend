import React, { useState, useEffect } from 'react';
import { Drawer, Row, notification } from 'antd';
import { EditTwoTone } from '@ant-design/icons';

import { FormComponent } from "../../../Components/Form";

export const SubjectForm = ({ id, onClose, visible, isAdmin }) => {
  const onFinish = (values) => {
    notification.info({
      message: `Subject ${id ? 'updated' : 'created'}`,
      description: JSON.stringify(values)
    });
  };

  const onFinishFailed = (errorInfo) => {
    notification.error({
      message: `Subject ${id ? 'update' : 'creation'} failed`,
      description: errorInfo
    });
  };

  const [disableField, setDisableField] = useState(false);
  const [subjectData, setSubjectData] = useState({
    subject_name: "",
    course_code: "",
    subject_code: "",
    year: null,
    semester: null,
    branch_name: "",
    books: []
  });

  useEffect(() => {
    if(id) {
      setDisableField(true);
      // Fire API for response and set state
      setSubjectData({
        subject_name: "Mathematics-I",
        course_code: "BTECH",
        subject_code: "M1",
        year: 2,
        semester: 1,
        branch_name: "ECE",
        books: [
          { title: "Mathematics-1", author: "Tannenbaum" }
        ]
      })
    }
  }, [id]);

  return (
    <Drawer
      closable
      width={400}
      placement="right"
      onClose={onClose}
      visible={visible}
      title="Add a Subject"
    >
      {id && isAdmin
      && (
        <Row justify="end">
          <EditTwoTone onClick={() => setDisableField(!disableField)} />
        </Row>
      )}
      <FormComponent
        name={`${id ? 'Update' : 'Add'} Subject`}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        submitButtonTitle={`${id ? 'Update' : 'Add'} this subject now`}
        formItems={[
          {
            initialValue: subjectData.subject_name,
            label: "Subject Name",
            name: "subject_name",
            rules: [{ required: true, message: 'Subject name is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: subjectData.course_code,
            label: "Course Name",
            name: "course_name",
            rules: [{ required: true, message: 'Course Name is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: subjectData.subject_code,
            label: "Branch Code",
            name: "subject_code",
            rules: [{ required: true, message: 'Branch code is required!' }],
            type: {
              name: 'input',
              props: { maxLength: 6, disabled: disableField }
            }
          },
          {
            initialValue: subjectData.year,
            label: "Year",
            name: "year",
            rules: [{ required: true, message: 'Year is required!' }],
            type: {
              name: 'input',
              props: { type: 'number', min: 2021, disabled: disableField }
            }
          },
          {
            initialValue: subjectData.semester,
            label: "Semeseter",
            name: "semester",
            rules: [{ required: true, message: 'Semeseter is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            label: "Books",
            type: {
              name: 'select',
              props: {
                defaultValue: subjectData.books.map(book => `${book && book.title} by ${book && book.author}`),
                placeholder: "Books",
                mode: "multiple",
                allowClear: true,
                options: subjectData.books,
              }
            }
          },
        ]}
      />
    </Drawer>
  )
}