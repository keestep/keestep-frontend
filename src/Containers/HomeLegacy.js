import React, { useEffect, useState } from "react";
import { Helmet } from 'react-helmet';
import styled from "styled-components";

import {
  FcHome,
  FcConferenceCall,
  FcCalendar,
  FcViewDetails,
  FcLibrary,
  FcDataSheet,
  FcLike,
  FcCustomerSupport,
  FcIdea,
  FcTemplate
} from 'react-icons/fc';
import { Layout, notification } from "antd";

// Containers
import { HomeDashboard } from "./HomeDashboard";
import { Users } from "./Users";
import Attendance from "./Attendance";
import Timetable from "./Timetable";
import NoticeBoard from "./NoticeBoard";
import Notifications from "./Notifications";
import { Queries } from "./Queries";
import { Marks } from "./Marks";
import { CollegeProfile } from "./CollegeProfile";
import { Students } from "./Students";
import { Faculty } from "./Faculty";
import { Books } from "./Books";
import { Courses } from "./Course";
// Components
import { GlobalSider } from "../Components/GlobalSider";
// APIs
import { getCurrentUser } from "../Api/AuthApi";
import { getNotificationCount } from "../Api/Notifications";

export const Home = () => {

  const profile = getCurrentUser(); 
  const user = profile && profile.data;
  const [option, setOption] = useState(<HomeDashboard profile={user} />);
  const [notificationCount, setNotificationCount] = useState(0);

  const fireApis = async () => {
    try {
      const { success, data } = await getNotificationCount();
      if (success) {
        setNotificationCount(data);
      }
    } catch (e) {
      notification.error({ message: e.message })
    }
  }

  useEffect(() => {
    fireApis();
  }, []);

  const menu = [
    {
      title: "Home",
      icon: <FcHome size={20} />,
      component: <HomeDashboard profile={user} setOption={setOption} />
    },
    {
      title: "Users", icon: <FcConferenceCall size={20} />,
      levels: [
        { title: "Students", component: <Students profile={user} /> },
        { title: "Faculty", component: <Faculty profile={user} /> },
        { title: "Administrators", component: <Users profile={user} /> },
      ]
    },
    {
      title: "College Setup", icon: <FcLibrary size={20} />,
      levels: [
        { title: "College Profile", component: <CollegeProfile /> },
        { title: "Subjects", component: <Users profile={user} /> },
        { title: "Books", component: <Books /> },
        { title: "Courses", component: <Courses /> },
        { title: "Branches", component: <Users profile={user} /> },
      ]
    },
    { title: "Attendance", icon: <FcViewDetails size={20} />, component: <Attendance /> },
    { title: "Timetable", icon: <FcCalendar size={20} />, component: <Timetable /> },
    { title: "Marks", icon: <FcDataSheet size={20} />, component: <Marks /> },
    { title: "Social", icon: <FcLike size={20} /> },
    { title: "Queries", icon: <FcCustomerSupport size={20} />, component: <Queries /> },
    { title: "Notice Board", icon: <FcTemplate size={20} />, component: <NoticeBoard /> },
    { title: "Notifications", icon: <FcIdea size={20} />, component: <Notifications />, count: notificationCount },
  ];

  return (
    <Layout>
      <Helmet>
        <title>Home</title>
        <meta name="description" content="Organize your students data" />
      </Helmet>
      <GlobalSider
        menu={menu}
        menuFunction={setOption}
        profile={user}
      />
      <StyledLayout>
        <MenuContent>
          {option}
        </MenuContent>
      </StyledLayout>
    </Layout>
  ) 
};

const StyledLayout = styled(Layout)`
  overflow: hidden;
  margin-left: 180px;
`;

const MenuContent = styled(Layout.Content)`
  margin: 24px 16px 0;
  width: 100%;
  height: 100%;
  // overflow-y: scroll;
  padding-right: 17px;
  box-sizing: content-box;
`;