import React, { useState, useEffect, useContext } from 'react';
import styled from 'styled-components';
import { message, Tabs } from 'antd';
import { LockOutlined, MailOutlined, ProfileOutlined } from '@ant-design/icons';

import login from '../../Assets/Img/login.svg'
import logo from '../../Assets/Img/logo.png'

import { FormComponent } from "../../Components/Form";
import { adminLogin } from "../../Api/AuthApi";
import { AuthContext } from '../../context/Context';

export const Login = () => {

  const { TabPane } = Tabs;
  const { authenticated } = useContext(AuthContext);
  const [loading, setLoading] = useState(false);
 
  const onAdminLogin = async ({email, password}) => {
    setLoading(true);
    try {
      const success = await adminLogin({ email: `${email};admin`, password });
      if(success) {
        window.location.href = "/";
      }
    }catch(error) {
      message.error(error.message);
    }
    setLoading(false);
  };

  const onFacultyLogin = async ({employeeId, password}) => {
    setLoading(true);
    try {
      const success = await adminLogin({ email: `${employeeId};faculty`, password });
      if(success) {
        window.location.href = "/";
      }
    }catch(error) {
      message.error(error.message);
    }
    setLoading(false);
  };

  const onFinishFailed = () => message.error('Login failed');

  useEffect(() => {
    if(authenticated) {
      window.location.href = "/";
    }
  }, [authenticated])

  const AdminForm = () => (
    <FormComponent
      name="Login"
      width={400}
      btnLoading={loading}
      onFinish={onAdminLogin}
      onFinishFailed={onFinishFailed}
      submitButtonTitle="Login"
      formItems={[
        {
          initialValue: null,
          label: "Email",
          name: "email",
          rules: [{ required: true, message: 'Email is required!' }],
          type: {
            name: 'input',
            props: { prefix: <MailOutlined />  }
          }
        },
        {
          initialValue: null,
          label: "Password",
          name: "password",
          rules: [{ required: true, message: 'Password is required!' }],
          type: {
            name: 'input',
            props: { type: "password", prefix: <LockOutlined />  }
          }
        },
      ]}
    />
  )

  const FacultyForm = () => (
    <FormComponent
      name="Login"
      width={400}
      btnLoading={loading}
      onFinish={onFacultyLogin}
      onFinishFailed={onFinishFailed}
      submitButtonTitle="Login"
      formItems={[
        {
          initialValue: null,
          label: "Employee ID",
          name: "employee_id",
          rules: [{ required: true, message: 'Employee ID is required!' }],
          type: {
            name: 'input',
            props: { prefix: <ProfileOutlined />  }
          }
        },
        {
          initialValue: null,
          label: "Password",
          name: "password",
          rules: [{ required: true, message: 'Password is required!' }],
          type: {
            name: 'input',
            props: { type: "password", prefix: <LockOutlined />  }
          }
        },
      ]}
    />
  )

  return (
    <Container>
      <Logo src={logo} alt="logo" width={50}/>
      <Left>
        <img src={login} width={200} alt="logo" />
        <Motto>Into the digital era</Motto>
      </Left>
      <Right>
        <Tabs defaultActiveKey="2">
          <TabPane tab="Faculty Login" key="1" >
            <FacultyForm />
          </TabPane>
          <TabPane tab="Admin Login" key="2">
            <AdminForm />
          </TabPane>
        </Tabs>  
      </Right>
    </Container>
  );
};

const Container = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  align-items: center;
  background-color: ${({ theme }) => theme.primary};
`;

const Left = styled.div`
  height: 100vh;
  width: 65%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: ${({ theme }) => theme.login};
`;

const Motto = styled.p`
  margin-top: 20px;
  font-size: 25px;
  color: white;
`;

const Logo = styled.img`
  position: absolute;
  left: 20px;
  top: 20px;
`;

const Right = styled.div`
  height: 100vh;
  width: 35%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${({ theme }) => theme.primary};
`;
