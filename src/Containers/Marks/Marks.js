import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { PageHeader, Input, Empty } from 'antd';
import ComingSoon from '../../Components/ComingSoon';

export const Marks = () => {  

  return (
    <Container>
      <Helmet>
        <title>Marks</title>
        <meta name="description" content="Upload, track and visualize marks" />
      </Helmet>
      <PageHeader title="Marks" />
      <ComingSoon />
    </Container>
  );
}

const Container = styled.div`
  height: 100vh;
`;