import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { EditFilled, DeleteFilled, UserOutlined, FieldTimeOutlined } from '@ant-design/icons';
import { Button, PageHeader, List, Avatar, Popconfirm, Empty } from 'antd';
import { NoticeBoardForm } from './components/NoticeBoardForm';
import { getNotices, updateNotice } from '../../Api/Noticeboard';

import { simplifyTime } from '../../Utility/TimeCalculate';
import NoticesSVG from '../../Assets/Img/Notifications.svg';

export default function NoticeBoard() {

  const [noticeFormVisible, setNoticeFormVisible] = useState(false);
  const [updateFormVisible, setUpdateFormVisible] = useState(false);
  const [noticeData, setNoticeData] = useState([]);
  const [changed, setChange] = useState(0)

  const fireApi = async () => {
    const res = await getNotices();
    setNoticeData(res.data);
  }

  const deleteNotice = async (id) => {
    const res = await updateNotice(id, {deleted: true});
    if(res.success) {
      setChange(1);
    }
  }

  const createForm = () => {
    setNoticeFormVisible(true);
  }

  const closeForm = () => {
    setNoticeFormVisible(false);
  }

  useEffect(() => {
    fireApi();
  }, [changed]);

  const cardActions = (id) => [
    <EditFilled key="edit" onClick={() => { 
        setUpdateFormVisible(true);
      }} 
    />,
    (
      <Popconfirm
        title="Are you sure you want to delete?"
        onConfirm={() => deleteNotice(id)}
      >
        <DeleteFilled key="delete" />
      </Popconfirm>
    ),
  ]

  const noticeInfo = (name, time) => (
    <NoticeItem>
      <UserItem>
        <UserOutlined />
        <Para> {name} </Para>
      </UserItem>
      <UserItem>
        <FieldTimeOutlined />
        <Para> {simplifyTime(time)} </Para>
      </UserItem>
    </NoticeItem>
  )

  return (
    <div style={{ height: '100vh' }}>
      <Helmet>
        <title>Notice Board</title>
        <meta name="description" content="Send notifications to students" />
      </Helmet>
      <PageHeader title="Notice Board" />
        
      {noticeData.length === 0
      ? (
        <EmptyArea>
          <Empty
            image={<img src={NoticesSVG} />}
            imageStyle={{ height: 250 }}
            description="You can send important notifications directly from here. Click to continue!"
          >
            <Button onClick={createForm} type="primary" > Create a Notice </Button>
            <NoticeBoardForm
              editMode={false}
              setChange={setChange}
              visible={noticeFormVisible}
              onClose={closeForm}
            />
          </Empty>
        </EmptyArea>
      )
      : (
          <ListContainer>
            <List
              itemLayout="vertical"
              size="small"
              dataSource={noticeData}
              renderItem={(item) => (
                <ListItem>
                  <List.Item
                    key={item.title}
                    actions={cardActions(item._id)}
                    extra={
                      <img
                        width={200}
                        alt="logo"
                        src={item.cover_image}
                      />
                    }
                  >
                    <List.Item.Meta
                      avatar={<Avatar src={item.created_by.photo_url} />}
                      title={item.title}
                      description={noticeInfo(item.created_by.name, item.created_at)}
                    />
                    {item.description}
                  </List.Item>
                  <NoticeBoardForm
                    editMode
                    setChange={setChange}
                    data={item}
                    visible={updateFormVisible}
                    onClose={() => setUpdateFormVisible(false)}
                  />
                </ListItem>
              )}
            />
          </ListContainer>
        )}
    </div>
  );
  }

const CreateButton = styled.div`
  display: flex;
  height: ${({ isEmpty }) => isEmpty ? "600px" : "0"};
  justify-content: ${({ isEmpty }) => isEmpty ? "center" : "flex-end"};
  align-items: ${({ isEmpty }) => isEmpty ? "center" : "none"};
  margin: 10px 60px;
`;

  const ListContainer = styled.div`
  margin: 50px auto;
  width: 80%;
`;

const ListItem = styled.div`
  margin: 10px 0;
  background-color: white;
  padding: 15px 15px 5px 15px;
  border-radius: 5px;
`;

const NoticeItem = styled.div`
  display: flex;
  justify-content: flex-start;
`;

const UserItem = styled.div`
  display: flex;
  align-items: baseline;
`;

const Para = styled.p`
  margin: 0 10px;
`;

const EmptyArea = styled.div` 
  display: flex;
  justify-content: center;
  align-items: center;
  height: 80vh;
`;