import React, { useState, useEffect } from 'react';
import styled from "styled-components";
import { Drawer, notification, Row, Typography } from 'antd';

import { FormComponent } from "../../../Components/Form";
import { ToggleEdit } from "../../../Components/ToggleEdit";
import { createNotice, updateNotice } from '../../../Api/Noticeboard';
import { getCourses } from '../../../Api/Course';

export const NoticeBoardForm = ({ data, onClose, visible, editMode, setChange }) => {

  const [loading, setLoading] = useState(false);
  const [disableField, setDisableField] = useState(editMode);
  const [noticeData, setNoticeData] = useState({
    ...data,
    courses: []
  });

  const getCoursesList = async () => {
    const res = await getCourses();
    setNoticeData({
      ...data,
      courses: res.data && res.data.map(item => ({
        value: item.course_name,
        name: item.course_name
      }))
    })
  }

  useEffect(() => {
    getCoursesList()
  }, [])

  const onFinish = async ({priority, link, attachments, cover_image , ...rest}) => {
    setLoading(true);
    const newNotice = {
      priority: parseInt(priority),
      link: `https://${link}.com`,
      attachments: [`https://${attachments}.com`],
      cover_image: `https://${cover_image}.photos/200`,
    }

    if(!editMode) { 
      console.log('kkkk', newNotice)
      const res = await createNotice({
        ...newNotice,
        ...rest
      });
      if(res.success) { 
        notification.success({ message: "Notice created Successfully !"});
        setChange(1);
      } 
    } else {
      const res = await updateNotice(data._id, {
        ...newNotice,
        ...rest
      })
      if(res.success) { 
        notification.success({ message: "Notice updated Successfully !"});
        setChange(1);
      }
      setNoticeData({
        ...res.data,
        courses: noticeData.courses
      });
    }
    setLoading(false);
  };


  return (
    <Drawer
      closable
      width={400}
      placement="right"
      onClose={onClose}
      visible={visible}
      title={`${editMode ? 'Update' : 'Add a'} Notice`}
    >
      {editMode && <ToggleEdit status={disableField} action={setDisableField} />}
      <FormComponent
        name={`${editMode ? 'Update' : 'Add'} Notice`}
        onFinish={onFinish}
        btnLoading={loading}
        btnDisabled={disableField}
        submitButtonTitle={`${editMode ? 'Update' : 'Add'} this notice now`}
        formItems={[
          {
            initialValue: editMode ? noticeData.title : "",
            label: "Title",
            name: "title",
            rules: [{ required: true, message: 'Title is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: editMode ? noticeData.description : "",
            label: "Description",
            name: "description",
            rules: [{ required: true, message: 'Description is required!' }],
            type: {
              name: 'textarea',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: editMode ? noticeData.link : "",
            label: "Link",
            name: "link",
            rules: [{ required: true, message: 'Link is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: editMode ? noticeData.priority : "",
            label: "Priority",
            name: "priority",
            rules: [{ required: true, message: 'Priority is required!' }],
            type: {
              name: 'input',
              props: { type: 'number', min: 1, max: 3, disabled: disableField }
            }
          },
          {
            initialValue: editMode ? noticeData.course_name : "",
            label: "Course",
            name: "course_name",
            rules: [{ required: true, message: 'Course is required!' }],
            type: {
              name: 'select',
              props: { 
                disabled: disableField,
                options: noticeData && noticeData.courses
              }
            }
          },
          {
            initialValue: editMode ? noticeData.cover_image : "",
            label: "Cover Image",
            name: "cover_image",
            rules: [{ required: true, message: 'Cover Image is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: editMode ? noticeData.attachments : "",
            label: "Attachments",
            name: "attachments",
            rules: [{ required: true, message: 'Attachments are required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          }
        ]}
      />
    </Drawer>
  )
}

const ToggleText = styled(Typography.Text)`
  margin-right: 15px;
`;

const StyledRow = styled(Row)`
  margin-bottom: 15px; 
`;