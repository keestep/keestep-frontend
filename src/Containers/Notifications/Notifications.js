import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import TimeDiff from 'js-time-diff';
import {
  List, Skeleton, PageHeader, Row, Button,
  Typography, Empty, notification, Spin
} from 'antd';

// api
import { getNotifications, markAsRead } from '../../Api/Notifications';
// utils
import { getAvatar } from '../../Utility/NotificationIconMap';

export default function Notifications() {

  const { Text, Link } = Typography;

  const [notifications, setNotifications] = useState([]);
  const [pageCount, setpageCount] = useState(1);
  const [hasMore, setHasMore] = useState(true);
  const [changed, setChanged] = useState(false);
  const [loading, setLoading] = useState(false);

  const fireApis = async () => {
    setLoading(true);
    try {
      const { success, data, message } = await getNotifications({
        page: pageCount
      });
      if(success) {
        setHasMore(data.length <= 10);
        setNotifications([...notifications, ...data]);
      } else {
        notification.error({ message });
      }
    } catch (e) {
      notification.error({ message: e.message });
    }
    setLoading(false);
  }

  const handleMarkAsRead = async (id) => {
    setLoading(true);
    await markAsRead(id);
    setChanged(!changed);
    setLoading(false);
  }

  const handleInfiniteOnLoad = () => {
    setpageCount(pageCount+1);
    fireApis();
  }

  useEffect(() => {
    fireApis();
  }, [changed]);

  if (notifications.length === 0 && !loading) {
    return (
      <EmptyDiv>
        <Empty description="No Notifications has been generated" />
      </EmptyDiv>
    );
  }

  const Loader = () => (
    <SpinnerContainer>
      <Spin tip="Loading Notifications" />
    </SpinnerContainer>
  )

  const LoadMore = () => (
    <Row justify="center">
      <Button onClick={handleInfiniteOnLoad}>
        Load More
      </Button>
    </Row>
  )

  return (
    <Container>
      <Helmet>
        <title>Notifications</title>
        <meta name="description" content="Find the most important updates here" />
      </Helmet>
      <PageHeader title="Notifications" />
      {loading
        ? (
          <Loader />
        )
        : (
          <StyledList
            loading={false}
            loadMore={hasMore && <LoadMore />}
            itemLayout="horizontal"
            dataSource={notifications}
            renderItem={({ _id, title, description, read, created_at }) => (
              <List.Item
                key={_id}
                actions={[
                  read
                    ? <Text type="secondary" italic>Read</Text>
                    : <Link onClick={() => handleMarkAsRead(_id)}>Mark as Read</Link>
                ]}
              >
                <Skeleton avatar title={false} loading={false} active>
                  <List.Item.Meta
                    avatar={getAvatar(title)}
                    title={title}
                    description={description}
                  />
                  <Text type="secondary" >
                    {TimeDiff(created_at, new Date())}
                  </Text>
                </Skeleton>
                {loading && hasMore && (
                    <Spin />
                )}
              </List.Item>
            )}
          />
        )
      }
    </Container>
  );
}

const Container = styled.div`
  height: 100vh;
  width: 100%;
`;

const StyledList = styled(List)`
  margin: 20px 30px;
`;

const EmptyDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

const SpinnerContainer = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
`;