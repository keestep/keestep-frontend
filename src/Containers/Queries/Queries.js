import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import TimeDiff from 'js-time-diff';  
import {
  PageHeader, Empty,
  Typography, Avatar,
  List, Space, Tag,
  notification, Row, Select
} from 'antd';
import { EyeOutlined } from '@ant-design/icons';

//images
import thumbnail from '../../Assets/Img/thumb-1.jpg';
// apis
import { getQueries } from '../../Api/Queries'
// components
import { QueryDetail } from './components/QueryDetail';
import { FilterStrip } from '../../Components/FilterStrip';
import { QUERY_STATUS, QUERY_TYPES, SORT_OPTIONS } from '../../Utility/constant';

export const Queries = () => {

  const { Text } = Typography;

  const [queries, setQueries] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [modalDetails, setModalDetails] = useState(null);
  const [sort, setSort] = useState(1);
  const [status, setStatus] = useState(QUERY_STATUS[0].name);

  const fireApis = async () => {
    try {
      const { success, data } = await getQueries({
        sort: parseInt(sort),
        status
      });
      if (success) {
        setQueries(data);
        // setModalDetails(data[0]);
      } else {
        notification.error({ message: "Couldn't fetch queries!" })
      }
    } catch (e) {
      notification.error({ message: e.message });
    }
  }

  useEffect(() => {
    fireApis();
  }, [sort, status]);

  return (
    <Container>
      <Helmet>
        <title>Queries</title>
        <meta name="description" content="Recieve and follow up queries from students" />
      </Helmet>
      <PageHeader title="Queries" />

      <FilterStrip
        filters={[
          {
            label: 'Sort: ',
            default: sort,
            action: setSort,
            options: SORT_OPTIONS,
          },
          {
            label: 'Status: ',
            default: QUERY_STATUS[0].name,
            action: setStatus,
            options: QUERY_STATUS,
          },
        ]}
      />

      {queries.length === 0
      ? (
        <EmptyArea>
          <Empty
            description="Well done! No queries."
          />
        </EmptyArea>
      )
      : (
        <MainArea>
          <StyledList
            itemLayout="vertical"
            size="large"
            dataSource={queries}
            renderItem={(data, i) => (
              <QueryCard
                key={i}
                clicked={modalDetails && (data.title == modalDetails.title)}
                actions={[
                  (
                    <Cursor onClick={() => {
                      setModalDetails(data)
                      setShowModal(true)
                    }}>
                      <Space>
                        <EyeOutlined />
                        View
                      </Space>
                    </Cursor>
                  ),
                ]}
              >
                <List.Item.Meta
                  avatar={<Avatar src={data.created_by.photo_url || thumbnail} size="large" />}
                  title={(
                    <Text>
                      {data.created_by.name}
                      {data.created_by.class_representative && <StyledTag color="#8e44ad">CR</StyledTag>}
                      <StyledTag color="#30336b">{QUERY_TYPES[data.type]}</StyledTag>
                      <StyledTag color="#10ac84">{status}</StyledTag>
                    </Text>
                  )}
                  description={`${data.created_by.course} ${data.created_by.branch}-${data.created_by.semester}${data.created_by.section}, Posted ${TimeDiff(data.created_at, new Date())}`}
                />
                <Text type="secondary" >{data.title}</Text>
              </QueryCard>
            )}
          />
          {modalDetails
          && <QueryDetail
                open={showModal}
                closeModal={() => setShowModal(false)}
                data={modalDetails}
              />
          }
        </MainArea>
      )}
    </Container>
  );
}

const Container = styled.div`
  padding: 15px;
`;

const EmptyArea = styled.div` 
  display: flex;
  justify-content: center;
  align-items: center;
  height: 80vh;
`;

const SelectQueryStatus = styled(Select)`
  width: 150px;
`;

const Cursor = styled.div`  
  cursor: pointer;
`;

const StyledTag = styled(Tag)`
  margin-left: 10px;
`;

const StyledList = styled(List)`
  width: 60%;
  overflow-y: auto;
  height: 80vh;
`;

const MainArea = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-start;
  margin-top: 20px;
`;

const QueryDetails = styled.div`
  width: 69%;
  background-color: white;
  height: 100%;
  overflow: hidden;
`;

const QueryCard = styled(List.Item)`
  background-color: ${({ clicked }) => clicked? 'rgba(58, 134, 255, 0.05)': 'white'};
  border-radius: 5px;
  cursor: pointer;
  margin: 20px 0;
`;