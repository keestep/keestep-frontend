import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import TimeDiff from 'js-time-diff';
import {
  Modal, List, Comment,
  Avatar, Typography, Tag,
  Space, Input, Form, Button,
  Upload, Empty, notification,
  Divider, Popconfirm,
} from 'antd';
import { DeleteFilled } from '@ant-design/icons';

//images
import thumbnail from '../../../Assets/Img/thumb-1.jpg';
// utils
import { COLOR_MAP, QUERY_TYPES } from '../../../Utility/constant';
// apis
import { replyToQuery, deleteReplyToQuery } from '../../../Api/Queries'
import { getCurrentUser } from '../../../Api/AuthApi';

export const QueryDetail = ({ open, closeModal, data }) => {

  const { Text, Paragraph } = Typography;
  const { TextArea } = Input;
  const currentUser = getCurrentUser();

  const [query, setQuery] = useState(data);
  const [reply, setReply] = useState('');

  const handleReply = async (values) => {
    try {
      const { success, data: queryData } = await replyToQuery(data._id, values);
      if (success) {
        setQuery(queryData);
        setReply('');
      }
    } catch (e) {
      notification.error({ message: e.message })
    }
  }

  const handleDeleteReply = async (queryId, replyId) => {
    try {
      const { success, data: queryData } = await deleteReplyToQuery(queryId, replyId);
      if (success) {
        setQuery(queryData);
      }
    } catch (e) {
      notification.error({ message: e.message })
    }
  }

  return (
    <Modal
      visible={open}
      width="60%"
      onCancel={closeModal}
      cancelText="Back"
    >
      <InnerContainer>
        <List
          dataSource={[query]}
          renderItem={({ type, created_by }) => (
            <List.Item>
              <List.Item.Meta
                avatar={<Avatar src={created_by.photo_url || thumbnail} size="large" />}
                title={(
                  <Text>
                    {created_by.name}
                    {created_by.class_representative && <StyledTag color="#8e44ad">CR</StyledTag>}
                    <StyledTag color="#30336b">{QUERY_TYPES[type]}</StyledTag>
                    <StyledTag color={COLOR_MAP[query.status]}>{query.status}</StyledTag>
                  </Text>
                )}
                description={`${query.created_by.course} ${query.created_by.branch}-${query.created_by.semester}${query.created_by.section}, Posted ${TimeDiff(data.created_at, new Date())}`}
              />
            </List.Item>
          )}
        />
        <Space direction="vertical" style={{ width: '90%', margin: '0 auto' }}>
          <Text strong>{query.title}</Text>
          <Paragraph type="secondary">
            {query.description}
          </Paragraph>

          <Divider orientation="left">Attachments</Divider>
          {query.attachments.length === 0
          ? (<Empty
              description={<Text type="secondary">No attachments</Text>}
            />)
          : (
            <Upload
              listType="picture-card"
              showUploadList={{ removeIcon: false }}
              fileList={query.attachments}
            />
          )}

          <Divider orientation="left">Comments</Divider>
          <Form
            onFinish={handleReply}
          >
            <Form.Item
              name="comment"
            >
              <TextArea
                value={reply}
                onChange={e => setReply(e.target.value)}
                placeholder={`Reply To ${query.created_by.name}`}
              />
            </Form.Item>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                disabled={reply === ''}
              >
                Reply
              </Button>
            </Form.Item>
          </Form>
          
          {query.replies.length === 0
          ? <Empty
              description={<Text type="secondary">No Replies yet. Be the first one to reply</Text>}
            />
          : <List
              dataSource={query.replies}
              renderItem={({ _id, comment, commented_by, commented_at }) => (
                <Comment
                  actions={currentUser._id===commented_by._id
                  ?[
                    <Popconfirm
                      title="Are you sure you want to delete?"
                      okText="Yes"
                      cancelText="No"
                      onConfirm={() => handleDeleteReply(query._id, _id)}
                    >
                      <Space><DeleteFilled />Delete</Space>
                    </Popconfirm>
                  ]: []}
                  author={commented_by.name}
                  datetime={new Date(commented_at).toUTCString()}
                  content={comment}
                  avatar={<Avatar src={commented_by.photo_url || thumbnail} size="small" />}
                />
              )}
            />
          }
        </Space>
      </InnerContainer>
    </Modal>
  );

}

const InnerContainer = styled.div`
  overflow-y: auto;
  // height: 80vh;
  padding: 15px;
  box-sizing: content-box;
  width: 100%;
`;

const StyledTag = styled(Tag)`
  margin-left: 10px;
`;