import React from 'react';
import { Helmet } from 'react-helmet';
import { PageHeader } from 'antd';
import ComingSoon from '../../Components/ComingSoon';

export default function Social () {
  return (
    <div style={{ height: '100vh' }}>
      <Helmet>
        <title>Social</title>
        <meta name="description" content="Mark, track and visualize attendance" />
      </Helmet>
      <PageHeader title="Social" />
      <ComingSoon />
    </div>
  );
}
