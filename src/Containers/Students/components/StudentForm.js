import React, { useState } from 'react';
import { Drawer, notification } from 'antd';

// utils
import { field, requiredString } from '../../../Utility/fieldMap';
// apis
import { createStudent } from '../../../Api/Student';
import { BLOOD_GROUPS, GENDER_TYPES } from '../../../Utility/constant';
// components
import { FormComponent } from '../../../Components/Form';

export const StudentEntryForm = ({ onClose, visible, makeChange }) => {

  const [loading, setLoading] = useState(false);

  const handleFinish = async (input) => {
    try {
      const { success } = await createStudent(input);
      if(success) {
        notification.success({ message: "Student enrolled successfully" })
      } else {
        notification.error({ message: "Unknown error while enrolling student" })
      }
    } catch (e) {
      notification.error({ message: e.message })
    }
  };

  const handleFailure = ({ errorFields }) => {
    errorFields.map(e => notification.error({ message: e.errors[0], duration: 2 }))
  };

  return (
    <Drawer
      closable
      width={450}
      placement="right"
      onClose={onClose}
      visible={visible}
      title="Enroll a Student"
    >
      <FormComponent
        onFinish={handleFinish}
        onFinishFailed={handleFailure}
        btnLoading={loading}
        submitButtonTitle="Submit"
        formItems={[
          {
            initialValue: '',
            label: field.reg_id.name,
            name: "reg_id",
            rules: [{ required: true }],
            type: {
              name: 'input',
              props: {
                prefix: field.name.icon
              }
            }
          },
          {
            initialValue: '',
            label: field.name.name,
            name: "name",
            rules: [{ required: true }],
            type: {
              name: 'input',
              props: {
                prefix: field.name.icon
              }
            }
          },
          {
            initialValue: '',
            label: field.email.name,
            name: "email",
            rules: [{ required: true }],
            type: {
              name: 'input',
              props: { 
                type: 'email',
                prefix: field.email.icon
              }
            }
          },
          {
            initialValue: '',
            label: field.course.name,
            name: "course",
            rules: [{ required: true }],
            type: {
              name: 'select',
              props: { 
                options: [],
                prefix: field.course.icon
              }
            }
          },
          {
            initialValue: '',
            label: field.branch.name,
            name: "branch",
            rules: [{ required: true }],
            type: {
              name: 'select',
              props: { 
                options: [],
                prefix: field.branch.icon
              }
            }
          },
          {
            initialValue: '',
            label: field.year.name,
            name: "year",
            rules: [{ required: true }],
            type: {
              name: 'number',
              props: { 
                prefix: field.year.icon
              }
            }
          },
          {
            initialValue: '',
            label: field.semester.name,
            name: "semester",
            rules: [{ required: true }],
            type: {
              name: 'select',
              props: { 
                options: [],
                prefix: field.semester.icon
              }
            }
          },
          {
            initialValue: '',
            label: field.section.name,
            name: "section",
            rules: [{ required: true }],
            type: {
              name: 'select',
              props: { 
                options: [],
                prefix: field.section.icon
              }
            }
          },
          {
            initialValue: '',
            label: field.year_of_intake.name,
            name: "year_of_intake",
            rules: [{ required: true }],
            type: {
              name: 'number',
              props: {
                  prefix: field.year_of_intake.icon,
                  min: 2000
              }
            }
          },
          {
            initialValue: '',
            label: field.year_of_passout.name,
            name: "year_of_passout",
            rules: [{ required: true }],
            type: {
              name: 'number',
              props: {
                  prefix: field.year_of_passout.icon,
                  min: 2000
              }
            }
          },
          {
            initialValue: '',
            label: field.class_representative.name,
            name: "class_representative",
            type: {
              name: 'checkbox',
              props: { 
              }
            }
          },
          {
            initialValue: '',
            label: field.address.name,
            name: "address",
            type: {
              name: 'textarea',
              props: { 
                prefix: field.address.icon
              }
            }
          },
          {
            initialValue: '',
            label: field.age.name,
            name: "age",
            rules: [{ required: true }],
            type: {
              name: 'number',
              props: {
                prefix: field.age.icon
              }
            }
          },
          {
            initialValue: '',
            label: field.blood_group.name,
            name: "blood_group",
            rules: [{ required: true }],
            type: {
              name: 'select',
              props: {
                options: BLOOD_GROUPS,
                props: { 
                  prefix: field.blood_group.icon
                }
              }
            }
          },
          {
            initialValue: '',
            label: field.gender.name,
            name: "gender",
            rules: [{ required: true }],
            type: {
              name: 'select',
              props: {
                options: GENDER_TYPES,
                props: { 
                  prefix: field.gender.icon
                }
              }
            }
          },
          {
            initialValue: '',
            label: field.interests.name,
            name: "interests",
            rules: [],
            type: {
              name: 'select',
              props: {
                options: [],
                mode: 'tags',
                props: { 
                  prefix: field.interests.icon,
                  type: 'multiple'
                }
              }
            }
          },
        ]}
      />
    </Drawer>
  )
}