import React, { useState } from 'react';
import styled from "styled-components";
import { PageHeader, Row, Typography, Col, Steps, List } from 'antd';

import { WEEKDAYS } from '../../Utility/constant';

export default function Timetable () {

  const { Title, Text } = Typography;
  const { Step } = Steps;

  const [tab, setTab] = useState(0);

  const handleChangeTab = current => setTab(current); 

  const firstTab = {
    mon: [
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "1.00pm", end_time: "2.00pm", subject: "Mathematics-1", faculty: "S Raghavendra", break: "Lunch" },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
    ],
    tue: [
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "1.00pm", end_time: "2.00pm", subject: "Mathematics-1", faculty: "S Raghavendra", break: "Lunch" },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
    ],
    wed: [
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "1.00pm", end_time: "2.00pm", subject: "Mathematics-1", faculty: "S Raghavendra", break: "Lunch" },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
    ], 
  }

  const secondTab = {
    thurs: [
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "1.00pm", end_time: "2.00pm", subject: "Mathematics-1", faculty: "S Raghavendra", break: "Lunch" },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
    ],
    fri: [
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "1.00pm", end_time: "2.00pm", subject: "Mathematics-1", faculty: "S Raghavendra", break: "Lunch" },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
    ],
    sat: [
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "1.00pm", end_time: "2.00pm", subject: "Mathematics-1", faculty: "S Raghavendra", break: "Lunch" },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
      { start_time: "10.30am", end_time: "11.30am", subject: "Mathematics-1", faculty: "S Raghavendra", break: null },
    ],
  }

  const timetable = {
    ...firstTab,
    ...secondTab
  }

  const renderTimetableList = (day) => (
    <Col span={6}>
      <List
        size="default"
        header={<WeekdayTitle>{WEEKDAYS[day]}</WeekdayTitle>}
        itemLayout="horizontal"
        dataSource={timetable[day]}
        renderItem={({ subject, faculty, start_time, end_time, break: breakName }) => (
          <List.Item>
            <List.Item.Meta
              title={<Title level={5}>{!breakName && `${start_time}-${end_time}`}</Title>}
              description={(
                <>
                  {!breakName
                    ? (
                      <>
                        <p>{subject}</p>
                        <p>{faculty}</p>
                      </>
                    )
                    : <p>{breakName}</p>
                  }
                </>
              )}
            />
            
          </List.Item>
        )}
      />
    </Col>
  )

  return (
    <Container>
      <PageHeader title="Timetable" />
      <Row justify="center">
        <StyledSteps current={tab} onChange={handleChangeTab}>
          <Step title="Mon-Wed" />
          <Step title="Thurs-Sat" />
        </StyledSteps>
      </Row>
      <StyledRow justify="space-around">
        {tab === 0
        ? ["mon", "tue", "wed"].map(day => renderTimetableList(day))
        : ["thurs", "fri", "sat"].map(day => renderTimetableList(day))
        }
      </StyledRow>
    </Container>
  );
}


const Container = styled.div`
  background-color: white;
`;

const StyledSteps = styled(Steps)`
  width: 50%;
`;

const StyledRow = styled(Row)`
  padding: 40px;
`;

const WeekdayTitle = styled(Typography.Title)`
  text-align: center;
`;