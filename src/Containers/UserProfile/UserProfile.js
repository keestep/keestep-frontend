import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import {
  PageHeader,
  Descriptions,
  Row,
  Col,
  message,
  Upload,
  Button,
  Alert,
  Space,
  Typography
} from 'antd';
import { FaUpload } from "react-icons/fa";

import { getAdmin } from '../../Api/Admin';
import { getCurrentUser } from '../../Api/AuthApi';
import thumbnail from '../../Assets/Img/thumb-1.jpg';
// components
import { Spacer } from "../../Components/Spacer";
import { UserForm } from "./components/UserForm";
// utils
import { field } from "../../Utility/fieldMap";

export const UserProfile = () => {

  const { Item } = Descriptions;
  const { Text } = Typography;

  const [user, setUser] = useState('');
  const [userId, setUserId] = useState(null);
  const [photo, setPhoto] = useState(null);
  const [editMode, setEditMode] = useState(false);

  const getUserApi= async () => {
    const id = getCurrentUser().data._id;
    const { data: adminData } = await getAdmin(id);
    const {
      _id, name, email, phone, role,
      college_code, photo_url, address,
      updated_at, updated_by
    } = adminData;
    setUserId(_id);
    setPhoto(photo_url)
    setUser({
      name, email, phone, role,
      college_code, address,
      updated_at: new Date(updated_at).toUTCString(),
      updated_by,
    });
  }

  useEffect(() => {
    getUserApi()
  },[]);
  
  const uploadProps = {
    name: 'file',
    onChange(info) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  return (
    <Container>
      <Helmet>
        <title>User Profile</title>
        <meta name="description" content="View, edit or delete the profile" />
      </Helmet>
      <PageHeader title="User Profile" />
      
      <Row justify="space-around">
        <Col span={8}>
          <ProfilePicContainer>
            <ProfilePic src={photo || thumbnail} />
            <Upload {...uploadProps} >
              <Button icon={<UploadIcon />}>Upload new picture</Button>
            </Upload>
          </ProfilePicContainer>
        </Col>
        <Col span={16}>
          <Spacer size={30} />
          <StyledRow justify="end">
            <Button type="primary" onClick={() => setEditMode(!editMode)}>
              {editMode ? "Back" : "Edit details"}
            </Button>
          </StyledRow>
          {!editMode
          ? (
            <Descriptions bordered>
              {Object.keys(user).map(key => (
                <Item
                  span={10}
                  label={(
                    <Space>
                      {field[key].icon}
                      <Text>{field[key].name}</Text>
                    </Space>
                  )}
                >
                  {user[key]}
                </Item>
              ))}
            </Descriptions>
          )
          : <UserForm
              id={userId}
              data={user}
              setEditMode={setEditMode}
              setUserData={setUser}
            />
          }
          
        </Col>
      </Row>
    </Container>
  );
}

const Container = styled.div`
  width: 100%;
  height: 95vh;
`;

const StyledRow = styled(Row)`
  margin-bottom: 20px;
  width: 70%;
`;

const UploadIcon = styled(FaUpload)`
  margin-right: 10px;
`;

const ProfilePic = styled.img`
  border-radius: 50%;
  width: 200px;
  margin-bottom: 30px;
`;

const ProfilePicContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const StyledAlert = styled(Alert)`
  width: 62%;
`;