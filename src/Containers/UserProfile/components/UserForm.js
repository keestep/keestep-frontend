import React, { useState, useEffect } from 'react';
import styled from "styled-components";
import { message, notification } from "antd";

import { FormComponent } from "../../../Components/Form";
import { updateAdmin } from "../../../Api/Admin";

export const UserForm = ({ id, data, setEditMode, setUserData }) => {

  const [loading, setLoading] = useState(false);

  const onFinish = async ({ role, ...values }) => {
    setLoading(true);
    const res = await updateAdmin(id, values);
    if(res && res.success) {
      notification.success({ message: "Profile updated successfully" });
      const { name, email, phone, role, college_code, address, updated_at, updated_by } = res.data;
      window.location.reload();
    }
    setLoading(false);
    setEditMode(false);
  };

  const onFinishFailed = (errorInfo) => {
    message.error(errorInfo);
  };

  return (
    <Container>
      <FormComponent
        name="Update Profile details"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        btnLoading={loading}
        submitButtonTitle="Save"
        formItems={[
          {
            initialValue: data && data.name,
            label: "Name",
            name: "name",
            rules: [],
            type: {
              name: 'input',
              props: {  }
            }
          },
          {
            initialValue: data && data.phone,
            label: "Phone",
            name: "phone",
            rules: [],
            type: {
              name: 'input',
              props: { type: 'tel' }
            }
          },
          {
            initialValue: data && data.address,
            label: "Address",
            name: "address",
            rules: [],
            type: {
              name: 'input',
              props: {  }
            }
          },
        ]}
      />
    </Container>
  );
}

const Container = styled.div`
  width: 70%;
`;