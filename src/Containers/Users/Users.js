import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { PageHeader, Typography, Row, Col, Spin, message } from "antd";
import { UserOutlined, LoadingOutlined } from "@ant-design/icons";

import { AdminForm } from "./components/AdminForm";
import { FacultyForm } from "./components/FacultyForm";
import { StudentForm } from "./components/StudentForm";
import { InfoCard } from "../../Components/InfoCard";

import { getAdmins } from "../../Api/Admin";
import { getFacultys } from "../../Api/Faculty";
import { getStudents } from "../../Api/Student";
import { AdminTable } from "../../Components/AdminTable";

export const Users = ({ profile }) => {
  const { Title } = Typography;
  const spinIcon = <LoadingOutlined style={{ fontSize: 30 }} spin />;

  const [adminFormVisible, setAdminFormVisible] = useState(false);
  const [facultyFormVisible, setFacultyFormVisible] = useState(false);
  const [studentFormVisible, setStudentFormVisible] = useState(false);

  const [admins, setAdmins] = useState([]);
  const [facultys, setFacultys] = useState([]);
  const [students, setStudents] = useState([]);

  const [loading, setLoading] = useState(false);
  const [makeChange, setMakeChange] = useState(false);

  const fireApis = async () => {
    
    const [adminRes, facultyRes, studentsRes] = await Promise.all([
      getAdmins(),
      getFacultys(),
      getStudents()
    ]);
    if(adminRes.success) {
      setAdmins(adminRes.data)
    } else {
      message.error(adminRes.message);
    }

    if(facultyRes.success) {
      setFacultys(facultyRes.data)
    } else {
      message.error(facultyRes.message);
    }

    if(studentsRes.success) {
      setStudents(studentsRes.data)
    } else {
      message.error(studentsRes.message);
    }
    
  };

  useEffect(() => {
    setLoading(true);
    fireApis();
    setLoading(false);
  },[makeChange]);

  return (
    <div style={{ height: "100vh" }}>
      <PageHeader title="Home" />
      <Container>
        {loading
        ? (
          <Row justify="center" style={{ height: 200 }}>
            <Spin indicator={spinIcon} />
          </Row>
          )
        : (
          <Row gutter={[0, 16]}>
            <Col span={6}>
              <InfoCard
                loading={loading}
                title={<a href="#students">Students</a>}
                description="students have enrolled in your college"
                color="beige"
                icon={<UserOutlined />}
                count={students.length}
                actionTitle="Enroll a student"
                setFormStatus={setStudentFormVisible}
              >
                <StudentForm
                  visible={studentFormVisible}
                  onClose={() => setStudentFormVisible(false)}
                />
              </InfoCard>
            </Col>
            <Col span={6}>
              <InfoCard
                loading={loading}
                title={<a href="#faculty">Faculty</a>}
                description="faculty members teaching in your college"
                color="azure"
                icon={<UserOutlined />}
                count={facultys.length}
                actionTitle="Add a faculty member"
                setFormStatus={setFacultyFormVisible}
              >
                <FacultyForm
                  visible={facultyFormVisible}
                  onClose={() => setFacultyFormVisible(false)}
                />
              </InfoCard>
            </Col>
            <Col span={6}>
              <InfoCard
                loading={loading}
                title={<a href="#admins">Admins</a>}
                description="Administrators to control your college app"
                color="ivory"
                icon={<UserOutlined />}
                count={admins.length}
                actionTitle="Add an Admin"
                setFormStatus={setAdminFormVisible}
              >
                <AdminForm
                  visible={adminFormVisible}
                  onClose={() => setAdminFormVisible(false)}
                />
              </InfoCard>
            </Col>
          </Row>
        )}
        
        <div style={{ height: 50 }} />
        <Row id="admins">
          <AdminTable data={admins} profile={profile} changed={setMakeChange} />
        </Row>
      </Container>
    </div>
  );
};

const Container = styled.div`
  padding: 0 22px;
`;
