import React, { useState, useEffect } from 'react';
import styled from "styled-components";
import generatePassword from 'password-generator';
import { Drawer, Row, notification, message, Typography } from 'antd';

import { FormComponent } from "../../../Components/Form";
import { ToggleEdit } from "../../../Components/ToggleEdit";
import { createAdmin, updateAdmin } from "../../../Api/Admin";

export const AdminForm = ({ data, onClose, visible, editMode, changed }) => {

  const [loading, setLoading] = useState(false);
  const [disableField, setDisableField] = useState(editMode);
  const [adminData, setAdminData] = useState(data);

  const onFinish = async ({ role, ...values }) => {
    setLoading(true);
    if(!data) {
      const res = await createAdmin({ password: generatePassword(6, false), ...values });
      if(res.success) {
        notification.success({ message: "Admin created successfully" });
        changed(1);
      }
    } else {
      const res = await updateAdmin(data._id, values);
      if(res.success) {
        notification.success({ message: "Admin updated successfully" });
        changed(1);
      }
    }
    setLoading(false);
  };

  const onFinishFailed = (errorInfo) => {
    message.error(errorInfo);
  };

  useEffect(() => {
    if(data) {
      setDisableField(true);
      setAdminData(data)
    }
  }, []);

  return (
    <Drawer
      closable
      width={400}
      placement="right"
      onClose={onClose}
      visible={visible}
      title={`${editMode ? 'Update' : 'Add an'} Admin`}
    >
      {data && <ToggleEdit status={disableField} action={setDisableField} />}
      <FormComponent
        name={`${editMode ? 'Update' : 'Add'} Admin`}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        btnLoading={loading}
        submitButtonTitle={`${editMode ? 'Update' : 'Add'} this admin now`}
        formItems={[
          {
            initialValue: editMode ? adminData.name : "",
            label: "Name",
            name: "name",
            rules: [{ required: true, message: 'Name is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: editMode ? adminData.email : "",
            label: "Email",
            name: "email",
            rules: [{ required: true, message: 'Email is required!' }],
            type: {
              name: 'input',
              props: { type: 'email', disabled: disableField }
            }
          },
          {
            initialValue: editMode ? adminData.phone : "",
            label: "Phone",
            name: "phone",
            rules: [],
            type: {
              name: 'input',
              props: { type: 'tel', disabled: disableField }
            }
          },
          {
            initialValue: editMode ? adminData.address : "",
            label: "Address",
            name: "address",
            rules: [],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: editMode ? adminData.role : "",
            label: "Role",
            name: "role",
            hidden: editMode,
            rules: [],
            type: {
              name: 'input',
              props: { hidden: editMode, disabled: true }
            }
          }
        ]}
      />
    </Drawer>
  )
}

const ToggleText = styled(Typography.Text)`
  margin-right: 15px;
`;

const StyledRow = styled(Row)`
  margin-bottom: 15px; 
`;