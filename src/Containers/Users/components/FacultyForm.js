import React, { useState, useEffect } from 'react';
import styled from "styled-components";
import generatePassword from 'password-generator';
import { Drawer, Row, notification, message, Switch, Typography } from 'antd';

// Components
import { FormComponent } from "../../../Components/Form";
// APIs
import { createFaculty, updateFaculty } from "../../../Api/Faculty";
// utils
import { BLOOD_GROUPS } from '../../../Utility/constant';

export const FacultyForm = ({ onClose, visible, data, editMode, changed }) => {

  const [loading, setLoading] = useState(false);
  const [disableField, setDisableField] = useState(editMode);
  const [facultyData, setFacultyData] = useState(data);

  const onFinish = async ({ role, ...values }) => {
    setLoading(true);
    if(!data) {
      const res = await createFaculty({ password: generatePassword(6, false), ...values });
      if(res.success) {
        notification.success({ message: "Faculty created successfully" });
        changed(1);
      }
    } else {
      const res = await updateFaculty(data._id, values);
      if(res.success) {
        notification.success({ message: "Faculty updated successfully" });
        changed(1);
      }
    }
    console.log({...values})
    setLoading(false);
  };

  const onFinishFailed = (errorInfo) => {
    message.error(errorInfo);
  };

  useEffect(() => {
    if(data) {
      setDisableField(true);
      setFacultyData({
        "emp_id": "string",
        "photo_url": "string",
        "email": "string",
        "name": "string",
        "subjects": "string",
        "branch": "string",
        "blood_group": "string",
        "is_HOD": true,
      })
    }
  }, []);

  return (
    <Drawer
      closable
      width={400}
      placement="right"
      onClose={onClose}
      visible={visible}
      title="Add a Faculty member"
    >
      {data
      && (
        <StyledRow justify="end">
          <ToggleText type="secondary">Toggle edit mode</ToggleText>
          <Switch checked={disableField} onChange={() => setDisableField(!disableField)} />
        </StyledRow>
      )}
      <FormComponent
        name={`${!!data ? 'Update' : 'Add'} Faculty`}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        btnLoading={loading}
        submitButtonTitle={`${!!data ? 'Update' : 'Add'} this member now`}
        formItems={[
          {
            initialValue: !!data ? facultyData.name : "",
            label: "Employee ID",
            name: "emp_id",
            rules: [{ required: true, message: 'Employee ID is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: !!data ? facultyData.is_HOD : "",
            label: "Toggle if this faculty member is a HOD?",
            name: "is_HOD",
            rules: [{ required: false }],
            type: {
              name: 'switch',
              props: {
                disabled: disableField
              }
            }
          },
          {
            initialValue: !!data ? facultyData.name : "",
            label: "Name",
            name: "name",
            rules: [{ required: true, message: 'Name is required!' }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: !!data ? facultyData.email : "",
            label: "Email",
            name: "email",
            rules: [{ required: true, message: 'Email is required!' }],
            type: {
              name: 'input',
              props: { type: 'email', disabled: disableField }
            }
          },
          {
            initialValue: !!data ? facultyData.photo_url : "",
            label: "Photo URL",
            name: "photo_url",
            rules: [{ required: false }],
            type: {
              name: 'input',
              props: { disabled: disableField }
            }
          },
          {
            initialValue: !!data ? facultyData.blood_group : "",
            label: "Blood Group",
            name: "blood_group",
            rules: [{ required: false }],
            type: {
              name: 'select',
              props: {
                defaultValue: null,
                placeholder: "Blood Group",
                allowClear: true,
                options: BLOOD_GROUPS,
                disabled: disableField
              }
            }
          },
        ]}
      />
    </Drawer>
  )
}

const ToggleText = styled(Typography.Text)`
  margin-right: 15px;
`;

const StyledRow = styled(Row)`
  margin-bottom: 15px; 
`;