import React from 'react';
import { Drawer } from 'antd';

export const StudentForm = ({ onClose, visible }) => {

  return (
    <Drawer
      closable
      width={400}
      placement="right"
      onClose={onClose}
      visible={visible}
      title="Enroll a Student"
    >
      
    </Drawer>
  )
}