import React from 'react';
import { Avatar } from 'antd';
import { DeleteFilled, EditFilled, FileAddFilled } from '@ant-design/icons';
import { FcFeedback } from 'react-icons/fc';

// utils
import { ACTION_MAP } from '../Utility/constant';

export const getAvatar = (action) => {
  switch (true) {
    case action.includes('Created'):
      return <Avatar icon={<FileAddFilled/>} style={{ backgroundColor: 'green' }} />;

    case action.includes('Updated'):
      return <Avatar icon={<EditFilled/>} style={{ backgroundColor: 'orange' }} />;
    
    case action.includes('Deleted'):
      return <Avatar icon={<DeleteFilled/>} style={{ backgroundColor: 'tomato' }} />;

    case action===ACTION_MAP.QUERY_CREATED:
      return <Avatar icon={<FcFeedback />} style={{ backgroundColor: 'snow' }} />;

    default:
      break;
  }
} 