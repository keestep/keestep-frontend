import axios from 'axios';
import { stringify } from 'qs';
import { notification } from 'antd';

import { API_HOST, ACCESS_KEY_CONSTANT } from '../Utility/constant';
import { getStorageItem } from '../Utility/storage';
import { Logout } from '../Api/AuthApi';

const token = getStorageItem(ACCESS_KEY_CONSTANT);

const API = axios.create({
  baseURL: API_HOST,
  headers: { 
    "access-control-allow-origin": "*",
    Authorization: `Bearer ${token}` 
  },
  paramsSerializer: params => stringify(params, { arrayFormat: 'brackets' })
});

API.interceptors.response.use((response) => response, (error) => {

  const statusCode = error.response.status;
  const message = error.response.data.message;

  if (statusCode === 400) {
    notification.error({
      message: "Invalid details submitted in form",
      description: message
    })
  }
  else if (statusCode === 401) {
    notification.error({
      message: "Authentication Failure",
      description: "Seems session was offline for long. Please login again"
    })
    Logout();
  }
  else if (statusCode === 403) {
    notification.error({
      message: "Authorization Failure",
      description: message
    })
  }
  else if (statusCode > 403 && statusCode < 500) {
    notification.error({
      message: message
    })
  }
  else if (statusCode === 500) {
    notification.error({
      message: "Internal Server Error",
      description: message
    })
  }
});

export default API;