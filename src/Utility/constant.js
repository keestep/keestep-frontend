export const API_HOST = process.env.REACT_APP_API_HOST;

export const ACCESS_KEY_CONSTANT = 'keestep_access_key';
export const CURRENT_USER_CONSTANT = 'keestep_current_user';

export const USER_ROLES = {
  admin: 'admin',
  faculty: 'faculty',
  student: 'student'
};

export const GENDER_TYPES = [
  { name: "male", value: "Male" },
  { name: "female", value: "Female" },
]

export const FORM_TYPES = {
  input: {
    name: 'input',
    radio: 'radio',
    number: 'number'
  },
}

export const WEEKDAYS = {
  mon: "Monday",
  tue: "Tuesday",
  wed: "Wednesday",
  thurs: "Thursday",
  fri: "Friday",
  sat: "Saturday"
}

export const BLOOD_GROUPS = [
  { name: true, value: "A+" },
  { name: "A-", value: "A-" },
  { name: "B+", value: "B+" },
  { name: "B-", value: "B-" },
  { name: "O+", value: "O+" },
  { name: "O-", value: "O-" },
  { name: "AB+", value: "AB+" },
  { name: "AB-", value: "AB-" },
]

export const SORT_OPTIONS = [
  { name: "Latest first", value: 1 },
  { name: "Oldest first", value: -1 },
]

export const QUERY_TYPES = {
  "feedback": "Feedback",
  "compliant": "Complaint",
  "ask-a-question": "Ask a Question"
}

export const QUERY_STATUS = [
  { name: "Open", value: "Open" },
  { name: "In Review", value: "In Review" },
  { name: "Closed", value: "Closed" },
]

export const COLOR_MAP = {
  "Open": "#10ac84",
  "In Review": "#ff9f43",
  "Closed": "#ee5253"
}

export const ACTION_MAP = {
  ADMIN_CREATED : 'Admin Profile Created',
  ADMIN_UPDATED : 'Admin Profile Updated',
  ADMIN_DELETED : 'Admin Profile Deleted',

  FACULTY_CREATED : 'Faculty Profile Created',
  FACULTY_UPDATED : 'Faculty Profile Updated',
  FACULTY_PROMOTE : 'Faculty Promoted',
  FACULTY_DELETED : 'Faculty Profile Deleted',

  STUDENT_CREATED : 'Student Profile Created',
  STUDENT_UPDATED : 'Student Profile Updated',
  STUDENT_PROMOTE : 'Student Promoted',
  STUDENT_DELETED : 'Student Profile Deleted',
  STUDENT_UPLOADED : 'Students Profile Created',

  COLLEGE_UPDATED : 'College Profile Updated',

  TIMETABLE_CREATED : 'TimeTable Created',
  TIMETABLE_UPDATED : 'TimeTable Updated',
  TIMETABLE_DELETED : 'TimeTable Deleted',

  NOTICEBOARD_CREATED : 'Noticeboard Created',
  NOTICEBOARD_UPDATED : 'Noticeboard Updated',
  NOTICEBOARD_DELETED : 'Noticeboard Deleted',

  BOOK_CREATED : 'Book uploaded',
  BOOK_UPDATED : 'Book Updated',
  BOOK_UPLOAD : 'Books uploaded',
  BOOK_DELETED : 'Book Deleted',

  BRANCH_CREATED : 'Branch Created',
  BRANCH_UPDATED : 'Branch Updated',
  BRANCH_DELETED : 'Branch Deleted',

  COURSE_CREATED : 'Course Created',
  COURSE_UPDATED : 'Course Updated',
  COURSE_DELETED : 'Course Deleted',

  SUBJECT_CREATED : 'Subject Created',
  SUBJECT_UPDATED : 'Subject Updated',
  SUBJECT_DELETED : 'Subject Deleted',

  POST_CREATED : 'Post Created',
  POST_UPDATED : 'Post Updated',
  POST_DELETED : 'Post Deleted',
  LIKED : 'Post is Liked',
  UNLIKED : 'Post is Unliked',
  COMMENT_CREATED : 'Comment Created',
  COMMENT_UPDATED : 'Comment Updated',
  COMMENT_DELETED : 'Comment Deleted',
  
  QUERY_CREATED : 'Query posted',
}