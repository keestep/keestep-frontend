import React from 'react';
import {
  InfoCircleFilled,
  ChromeFilled,
  MailFilled,
  PhoneFilled,
  EditFilled,
  QuestionCircleFilled,
} from "@ant-design/icons";
import {
  FaMap,
  FaUserCog,
  FaUniversity,
} from "react-icons/fa";
import {
  GiWaterDrop
} from "react-icons/gi";

const commonStyles = {
  color: 'gray',
  size: 15
}

export const field = {
  name: { name: "Name", icon: <InfoCircleFilled {...commonStyles} /> },
  email: { name: "Email", icon: <MailFilled {...commonStyles} /> },
  college_name: { name: "College Name", icon: <InfoCircleFilled {...commonStyles} /> },
  college_code: { name: "College Code", icon: <FaUniversity {...commonStyles} /> },
  address: { name: "Address", icon: <FaMap {...commonStyles} /> },
  website: { name: "Website", icon: <ChromeFilled {...commonStyles} /> },
  reg_id: { name: "Registration ID", icon: <ChromeFilled {...commonStyles} /> },
  course: { name: "Course", icon: <ChromeFilled {...commonStyles} /> },
  branch: { name: "Branch", icon: <ChromeFilled {...commonStyles} /> },
  year: { name: "Current Year", icon: <ChromeFilled {...commonStyles} /> },
  semester: { name: "Semester", icon: <ChromeFilled {...commonStyles} /> },
  section: { name: "Section", icon: <ChromeFilled {...commonStyles} /> },
  year_of_intake: { name: "Joining Year", icon: <ChromeFilled {...commonStyles} /> },
  year_of_passout: { name: "Passout Year", icon: <ChromeFilled {...commonStyles} /> },
  age: { name: "Age", icon: <ChromeFilled {...commonStyles} /> },
  gender: { name: "Gender", icon: <ChromeFilled {...commonStyles} /> },
  role: { name: "Role", icon: <FaUserCog {...commonStyles} /> },
  interests: { name: "Interests", icon: <ChromeFilled {...commonStyles} /> },
  phone: { name: "Phone Number", icon: <PhoneFilled {...commonStyles} /> },
  blood_group: { name: "Blood Group", icon: <GiWaterDrop {...commonStyles} /> },
  class_representative: { name: "Class Representative", icon: <GiWaterDrop {...commonStyles} /> },
  course_name: { name: "Course Name", icon: <GiWaterDrop {...commonStyles} /> },
  course_code: { name: "Course Code", icon: <GiWaterDrop {...commonStyles} /> },
  duration: { name: "Duration(In Years)", icon: <GiWaterDrop {...commonStyles} /> },
  credits: { name: "Credits", icon: <GiWaterDrop {...commonStyles} /> },
  updated_at: { name: "Last Updated", icon: <EditFilled {...commonStyles} /> },
  updated_by: { name: "Last Updated by", icon: <QuestionCircleFilled {...commonStyles} /> },
}