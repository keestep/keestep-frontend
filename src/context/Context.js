import { createContext } from 'react';
import { ACCESS_KEY_CONSTANT } from '../Utility/constant';

const isTokenPresent = localStorage.getItem(ACCESS_KEY_CONSTANT) && localStorage.getItem(ACCESS_KEY_CONSTANT).length>0

export const AuthContext = createContext({ 
  authenticated: isTokenPresent
});